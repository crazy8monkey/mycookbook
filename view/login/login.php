<form ng-submit="loginFormSubmit()">
	<div class="content loginEmailForm">
		<div class="row pageMessage" ng-show="showMessage">
			<div class="col-md-12">
				<div class='loadingSpinner' ng-show="loadingFinal">
					<img ng-src="{{ finishLoading }}" />
				</div>
				<div class='text alert alert-danger'>
					{{errorMessage}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="formLabel">Username</div>
					<input type="text" name="userNameLogin" ng-model="userNameLogin">
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="formLabel">Password</div>
					<input type="password" name="passwordLogin" ng-model="passwordLogin">
				</div>	
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-md-12">
				<input type="submit" value="LOGIN" class="blueButton" style="margin-right:10px;" >
				<span>New To MyCookBook?</span> <a style="text-decoration:underline;" href="#signup">Sign Up Now</a>
			</div>
		</div>
			
		<div class="row socialMediaLink">
			<div class="col-md-12">
				<div class="faceBookIntegration">
					Login with Facebook	
					<div class="line"></div>
				</div>
			</div>
		</div>
			
		<div class="row">
			<div class="col-md-12" style="border-top:1px solid #f7f7f7; margin-top:20px; padding-top:5px;">
				<a style="text-decoration:underline;" href="#credentials">Forgot Credentials?</a>		
			</div>
		</div>
			
			
			
	</div>	
			
</form>

