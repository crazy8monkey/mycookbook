<form ng-submit="credentialsFormSubmit()" style="position:relative;">
	<div class="content loginEmailForm forgotCredentialsForm" ng-hide="hideForm">
		<div class="welcomeText">
			Forgot your credentials?	
		</div>
		<div class="retrieveCredientialsText">
			Please enter your email in the inbox below.<br />
			If your email is valid/registered into our system,<br />
			Your email credentials will be emailed to you.				
		</div>
		<div class="row pageMessage" ng-show="showMessage">
			<div class="col-md-12">
				<div class='loadingSpinner' ng-show="loadingFinal">
					<img ng-src="{{ finishLoading }}" />
				</div>
				<div class='text alert alert-danger'>
					{{errorMessage}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="inputLine">
					<div class="formLabel">Your Email</div>
					<input type="text" name="userEmail" ng-model="userEmail">
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" value="SUBMIT" class="blueButton" />
			</div>
		</div>
	</div>
	
	
	<!--finished content!-->
	<div class="content loginEmailForm credentialsSent" ng-show="hideForm">
		<div class="welcomeText">
			Your credentials have been sent	
		</div>
		<div class="retrieveCredientialsText">
			Thank you for submitting your email.<br />
			Your credentials has been sent to your email: <strong>{{userEmail}}</strong>.<br /><br />
			Please check your email/junk filters to retrieve your login credentials.	
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" value="LOGIN" class="blueButton" ng-click="loginClick()" />
			</div>
		</div>
		
		
	</div>
	
	
</form>	


