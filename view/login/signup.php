<form ng-submit="signupFormSubmit()">
	<div class="content">
		<div class="welcomeText">
			Your recipe book starts now	
		</div>
		<div class="row pageMessage" ng-show="showMessage">
			<div class="col-md-12">
				<div class='loadingSpinner' ng-show="loadingFinal">
					<img ng-src="{{ finishLoading }}" />
				</div>
				<div class='text alert alert-danger'>
					{{errorMessage}}
				</div>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-6">
				<div class="inputLine">
					<div class="formLabel">First Name</div>
					<input type="text" name="firstName" ng-model="firstName" ng-focus="firstNameFocus">
				</div>
			</div>
			<div class="col-md-6">
				<div class="inputLine">
					<div class="formLabel">Last Name</div>
					<input type="text" name="lastName" ng-model="lastName">
				</div>
			</div>	
		</div>
		<div class='row'>
			<div class="col-md-6">
				<div class="inputLine">
					<div class="formLabel">Username</div>
					<input type="text" name="userName" ng-model="userName">
				</div>
			</div>
			<div class="col-md-6">
				<div class="inputLine">
					<div class="formLabel">Email</div>
					<input type="text" name="email" ng-model="email">
				</div>
			</div>					
		</div>				
		<div class='row'>
			<div class="col-md-6">
				<div class="inputLine">
					<div class="formLabel">Password</div>
					<input type="password" name="password" ng-model="password">
				</div>
			</div>
			<div class="col-md-6">
				<div class="inputLine">
					<div class="formLabel">Re-type Password</div>
					<input type="password" name="confirmPassword" ng-model="confirmPassword">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" class="blueButton" value="SAVE ACCOUNT" style="margin-right:10px;" />			 
				<span>Have an account?</span> <a style="text-decoration:underline;" href="#login">Login</a>						
			</div>
		</div>
		<div class="row socialMediaLink">
			<div class="col-md-12">
				<div class="faceBookIntegration">
					Join with Facebook	
					<div class="line"></div>
				</div>
			</div>
		</div>		
	</div>
</form>