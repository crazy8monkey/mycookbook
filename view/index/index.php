
<div class="entry-text-container">
	<div class="entry-text-link-section">
		<div class="recipe-book-icon"></div>
		<div class="signupLogin-links">
			<span style="font-size: 65px; line-height: 60px;">The last cook book</span> <br />
			<span style="font-size: 25px; line-height: 0px;">you will ever need.</span>
		</div>
		<div class="recipe-description">
			Take your handwritten recipe and store it online.
			Get rid of the hassle of losing that great recipe, and have it stored in one location!
		</div>

		<div class="credits">
			<p>2013 - <?php echo date("Y") ?> &copy; <a href="http://www.adamistheschmidt.com/" target="_blank">Adam Schmidt.</a> All Rights Reserved.</p>
		</div>
	</div>
</div>





<div class="background-slideshow"></div>

