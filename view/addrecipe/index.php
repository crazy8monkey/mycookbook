<div class="wrapper" style="width:768px">
<script type="text/javascript">
	tinyMCE.init({
		
		selector: "textarea[name='recipeIngredients'], textarea[name='recipeInstructions']",
		menubar : false,
		
		onchange_callback : function(editor) {
			tinyMCE.triggerSave();
			$("#" + editor.id).valid();
		},
		setup: function(ed) {
        	ed.on('init', function() {
        	    this.getDoc().body.style.fontSize = '12px';
    	    });
    	    ed.on('change', function(e) {
            	console.log(e);
            	$(ed).valid();
        	});
 	   },
	});
</script>

<div class="recipe-white-box-section">
	<div class="normal-header">
		<h1>Add New Recipe</h1>
	</div>
	<div class="breadcrumbs">
		<div class="link"><a href="<?php echo PATH ?>recipes">Home</a></div>
		<div class="breadcrumb-divider"></div>
		<div class="link"><strong>Add New Recipe</strong></div>
		<div style="clear:both;"></div>
	</div>
	<div class="white-box-content" style="width: 768px; padding: 8px 0px 0px 0px;">	
		<form enctype="multipart/form-data" id="add-new-recipe-form" name="addNewRecipeForm" method="post" action="<?php echo PATH ?>addrecipe/newRecipe">
			<div class="form-container" style="margin-top:0px;">
				<div class="form-row">
					<div class="form-input">
						<div class="form-label">Upload Recipe Image</div>
						<input type="file" id='image-upload' name="recipeImage" style="border:none;" /><a href="javascript:void(0);" onclick="MainApp.clearFileInput();" class="clearFileInput">x clear</a>
						<div style="float:right;">
							<span class="new-required" style="margin-top: 3px;">(.jpg, .jpeg, .png, .gif only)</span>
						</div>
						
					</div>
				</div>
				<div class="form-row">
					<div class="form-input">
						<div class="form-label">Category</div>
						<select name="recipeCategory" id="recipeCategory">
							<?php 
							foreach ($this->recipeCategories as $key => $value) 
							{
							?>
							<option value="<?php echo $value['category_id']?>"><?php echo $value['categoryName']?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>	
				<div class="form-row">
					<div class="form-input">
						<div class="form-label" style="margin-top:8px;"><span class="required"><strong>Recipe Name</strong> *</span></div>
						<div style="float:left;">
							<input id="recipe-name" class="required" type="text" name="recipeName" value="<?php echo $this -> recipeName; ?>" />	
						</div>
						<div style="clear:both"></div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-input">
						<div class="form-label" style="margin-top:8px;"><span class="required"><strong>Recipe Ingredients</strong> *</span></div>
						<div style="margin-left: 170px;">
							<textarea name="recipeIngredients" id="recipeIngredients" class="required" >
								<?php echo $this -> recipeIgredients; ?>
							</textarea>
						</div>
					</div>
				</div>
				<div class="border-divide"></div>
				<div class="form-row">
					<div class="form-input">
						<div class="form-label" style="margin-top:8px;"><span class="required"><strong>Recipe Instructions</strong> *</span></div>
						<div style="margin-left: 170px;">
							<textarea name="recipeInstructions" id="recipeInstructions" class="required"></textarea>
						</div>
					</div>
				</div>
				
			</div>
			<div class="button-holder">
				<input type='submit' id="submit" name='newRecipeSubmit' value='Submit' />
			</div>
		</form>
	</div>
</div>
</div>