<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en" <?=(isset($this->angular)) ? 'ng-app="myCookBookApp"' . $this->angular : ''; ?> > <!--<![endif]-->
	<head>
		<title ng-bind="title"></title>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo PATH ?>public/font/style.css" type="text/css" />
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
		<?php if(isset($this->css))  {
			foreach ($this -> css as $css)  {	?>
		<link rel="stylesheet" href="<?php echo PATH  ?>public/css/<?php echo $css; ?>" />
		<?php }
		}
		?>
		
		<link href="//cdn.muicss.com/mui-0.6.8/css/mui.min.css" rel="stylesheet" type="text/css" />
    	<script src="//cdn.muicss.com/mui-0.6.8/js/mui.min.js"></script>
		<script type="text/javascript" src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js" type="text/javascript"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.min.js" type="text/javascript"></script>		
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-animate.js" type="text/javascript"></script>
		
		<script src="<?php echo PATH ?>public/js/angular.tinymce.js" type="text/javascript" type="text/javascript"></script>
		
		<script src="<?php echo PATH ?>public/js/globals.js" type="text/javascript" type="text/javascript"></script>
		<script src="<?php echo PATH ?>public/js/AngularApp.js" type="text/javascript" type="text/javascript"></script>
		
		<script type="text/javascript" src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
		
		
		
		<!--[if lt IE 9]>
			<script src="html5shiv.js" type="text/javascript"></script>
		<![endif]-->
		<?php if(isset($this->js))  {
			foreach ($this -> js as $js) {	?>
				<script type="text/javascript" src="<?php echo $js; ?>"></script>
			<?php }
		}
		?>
		<script type="text/javascript"> 
			document.createElement("section");
			document.createElement("header");
			document.createElement("footer");
			
			//Globals.AngularVariable()
			
			
			<?php if(isset($this->AngularMethod)) : ?>
				<?php foreach ($this -> AngularMethod as $method) : ?>	
					<?php echo $method; ?>	
				<?php endforeach; ?>
			<?php endif; ?>
			
			
			
			$(document).ready(function(){
				<?php if(isset($this->startJsFunction)) : ?>
					<?php foreach ($this -> startJsFunction as $startJsFunction) : ?>	
						<?php echo $startJsFunction; ?>	
				<?php endforeach; ?>
				<?php endif; ?>
				
				Globals.closeFlashMessageFade();
				Globals.closePopups(".passwordSentPopup, .deleteAccountPopup, .errorSuggestionBox");
			});
		</script>
	</head>
<body <?=(isset($this -> bodyClass)) ? '' . $this -> bodyClass . '"' : ''; ?>>

	
<header>
	<div ng-class="{'container-fluid': fullPage, 'container': normalPage}">
		<div class="row content">
			
			<!--Logo-->
			<div class='col-xs-6 col-sm-6 col-md-8'>
				<a ng-href="{{homeLink}}">
					<img src="<?php echo PATH ?>public/images/home-logo.png" border="0">
				</a>	
			</div>
			
			<!--Login Links!-->
			<div class='col-xs-6 col-md-4 loginSignupButtons' ng-show="loginControllerButtons">
				<a href="<?php echo $this -> pages -> LoginController() ?>#login" class="OrangeButton hidden-xs">LOGIN</a>
				<a href="<?php echo $this -> pages -> LoginController() ?>#signup" class="OrangeButton hidden-xs">SIGN UP</a>
				
				<div class="dropDownElement visible-xs">
					<a href="javascript:void(0);" onclick="Globals.mobileDropDown()">
						<div class="bars"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></div>						
					</a>
					<div class="dropDown">
						<a href="<?php echo $this -> pages -> LoginController() ?>#login"><div class="link">Login</div></a>
						<a href="<?php echo $this -> pages -> LoginController() ?>#signup"><div class="link">Sign Up</div></a>					
					</div>
				</div>
			</div>
			
			<!--Recipe Main App Links!-->
			<div class='col-xs-6 col-md-4 recipeMainAppButtons' ng-show="recipeMainAppButtons">
				<a href="<?php echo $this -> pages -> recipeMainApp()?>#/my-recipe-book" class="whiteButton hidden-xs">YOUR RECIPES</a>
				<a href="<?php echo $this -> pages -> recipeMainApp()?>#/add-recipe" class="CallToActionGreenButton hidden-xs">+ RECIPE</a>
				
				<div class="dropDownElement">
					<a href="javascript:void(0);" onclick="Globals.mobileDropDown()">
						<div class="bars"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></div>						
					</a>
					<div class="dropDown">
						<a href="<?php echo $this -> pages -> recipeMainApp() ?>#/my-recipe-book" class="visible-xs"><div class="link">Your Recipes</div></a>
						<a href="<?php echo $this -> pages -> recipeMainApp() ?>#/add-recipe" class="visible-xs"><div class="link">Add New Recipe</div></a>
						<a href="<?php echo $this -> pages -> recipeMainApp() ?>#/account/settings"><div class="link">Profile</div></a>
						<a href="<?php echo $this -> pages -> errorMainPage() ?>"><div class="link">Errors / Suggesions</div></a>
						<a href="<?php echo $this -> pages -> logout() ?>"><div class="link">Logout</div></a>								
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<div class="pageTitleHeader" ng-show='showPageTitleHeader'>
		<div ng-class="{'container-fluid': fullPage, 'container': normalPage}" >
			<div class="addRecipeBreadCrumbs" ng-show="recipeController">
				<a href="<?php echo $this -> pages -> recipeMainApp() ?>#/my-recipe-book">Your Recipes</a>
				<div class="currentPage">New Recipe</div>
			</div>
			<div class="loginPageTitle" ng-bind="PageTitleHeader" ng-show="loginController"></div>
			
		</div>
	</div>	
</header>
<div class="headerPush"></div>



