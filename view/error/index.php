<div class="greyBackgroundContent">
		<div class='container entryContent'>
			<div class="row">
				<div class="col-md-12">
					Encountered an error? <br />
have a suggestion to make MyCookBook better? <br /><br />

Please fill out this form, and we will look into it.<br />
					<strong>
						Thank you for being a part of the MyCookBook Community!
					</strong>
				</div>
				
			</div>
			
		</div>	
	</div>
	
	<div class='container '>
		<form ng-submit="errorFormSubmit()">
			<div class="content">
				<div class="row pageMessage" ng-show="showMessage">
					<div class="col-md-12">
						<div class='loadingSpinner' ng-show="loadingFinal">
							<img ng-src="{{ finishLoading }}" />
						</div>
						
						<div ng-class="{'alert-danger': errorMessageShow, 'alert-success': successMessageShow}" class='text alert'>
							{{errorMessage}}
						</div>
					</div>
				</div>
				<div class="row" ng-hide="SessionContent">
					<div class="col-md-6">
						<div class="inputLine">
							<div class="formLabel">Full Name</div>
							<input type="text" name="errorFullName" ng-model="errorFullName">
						</div>
					</div>
					<div class="col-md-6">
						<div class="inputLine">
							<div class="formLabel">Email</div>
							<input type="text" name="errorEmail" ng-model="errorEmail">
						</div>
					</div>			
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class='inputLine'>
							<div class="formLabel">Error Type</div>
							<div class='mui-select'>
								<select name='errorType' ng-model='errorType'>
									<option value='0'>Please Select</option>
									<option ng-repeat='x in categories' value='{{x.id}}'>{{x.ErrorCategory}}</option>
								</select>
							</div>
						</div>	 
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="inputLine">
							<div class="formLabel">Description</div>
							<textarea name='errorDescription' ui-tinymce="tinymceOptions" class="tinyMCE" ng-model="errorDescription"></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" class="greenButton" value="SUBMIT" />
					</div>
				</div>
			</div>
		</form>
	</div>

