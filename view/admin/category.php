<div class="wrapper">
	<div class="headerPage" style="clear:both; height:30px;">
		<div style="float:left">
			Categories	
		</div>
	</div>
	<div class="breadCrumbs">
		<div class="link">Home</div>
		<div class="breadcrumb-arrow"></div>
		<div class="link">Categories</div>
		<div style="float:right; position:relative;">
			<a href="javascript:void(0);" onclick="AdminController.toggleNewCategoryForm();">
				<div class="newSection" style="text-decoration:none; margin-top: -10px; margin-right: -10px;">
					New Category	
				</div>
			</a>
			<div class="new-category-form">
				<form id="newCategory" action="<?php echo PATH ?>admin/save/errorCategory" method="post">
					<div class="form-container">
						<div class="input-row">
							New Category
							<input type="text" style="width: 212px; margin-top:10px;" name="newCategory">
						</div>		
					</div>
					<div style="border-top:1px solid #d4d4d4;"></div>	
					<div class="submit-button-section">
						<input type="submit" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php require "view/errorSubList.php" ?>
	<div id="categoryLists" style="margin-left:215px;">
		<?php 
		foreach ($this->errorCategoryList as $key => $value) 
		{
		?>
		<a class="adminListLineLink" href="javascript:void(0);" rel="<?php echo $value['id']?>">
			<div class="adminListLine">
				<div style="float:left">
					<?php echo $value['ErrorCategory'] ?>	
				</div>
			</div>
		</a>
			
		<?php 
		} 
		?>
		<div id="noCategoryText">
			There are no Error Catories entered	
		</div>
	</div>
</div>
