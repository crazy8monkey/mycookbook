<div class="wrapper">
	<div class="headerPage" style="clear:both; height:30px;">
		<div style="float:left">
			Error Ticket No: MCB - <?php echo $this -> error -> errorID ?>	
		</div>
		
	</div>
	<div class="breadCrumbs" style="clear:both;">
		<div class="link">Home</div>
		<div class="breadcrumb-arrow"></div>
		<div class="link"><a class="breakcrumbLink" href="<?php echo PATH ?>admin/errors?page=1">Errors</a></div>
		<div class="breadcrumb-arrow"></div>
		<div class="link">MCB - <?php echo $this -> error -> errorID ?></div>
		<div style="float:right">
			<a class="delete" href="<?php echo PATH ?>admin/delete/<?php echo $this -> error -> errorID; ?>/errors">
				Delete
			</a>
		</div>	
	</div>
	<div class="adminListLine" style="height:auto; padding-top: 0px;">
		<div class="adminLabel">
			<strong>Submitted by</strong>
		</div>
		<div class="adminData">
			<?php echo $this -> error -> name ?>	
		</div>
	</div>
	<div class="adminListLine" style="height:auto;">
		<div class="adminLabel">
			<strong>Email</strong>
		</div>
		<a style="color:#2a2a2a" href="mailto:<?php echo $this -> error -> email ?>"><?php echo $this -> error -> email ?></a>
	</div>
	<div class="adminListLine" style="height:auto;">
		<div class="adminLabel">
			<strong>Date</strong>
		</div>
		<div class="adminData">
			<?php echo $this -> recordedTime -> formatDate($this -> error -> enteredDate, $this -> settingTimeZone) . ' / ' . $this -> recordedTime -> formatTime($this -> error -> enteredTime, $this -> settingTimeZone)?>	
		</div>
	</div>
	<div class="headerPage" style="border-bottom: 1px solid #d9d9d9; padding: 10px;">
		Ticket Description
	</div>
	
	<div class="adminListLine" style="height: 40px;">
		<div style="float:left">
			<div class="adminLabel">
				<strong>Type</strong>
			</div>
			<div class="adminData" style="height: 18px;">
				<span id="errorTypeChanged"><?php echo $this -> error -> errorType ?></span>	
			</div>
		</div>	
		<div class="editError-type">
			<div class="editErrorCategory">
				<form id="categoryChange" action="<?php echo PATH ?>admin/errorCatoryEdit/<?php echo $this -> error -> errorID?>" method="post" style="float: left; margin: 13px 10px 10px 25px;">
					<select name="ticketCategory" id="ticketCategory">
						<?php 
						foreach ($this -> categories as $key => $value) 
						{
						?>
						<option value="<?php echo $value['ErrorCategory']; ?>" <?php if($this -> error -> errorType == $value['ErrorCategory']) echo 'selected'; ?>><?php echo $value['ErrorCategory']; ?></option>
						<?php }?>	
					</select>
					<input type="submit" style="padding:5px; margin-left: 10px; font-size:12px;" value="Submit"/>
				</form>
				<a href="javascript:void(0);" onclick="AdminSection.closeEditCategory();"><div class="closeEditCateogry"></div></a>
			</div>
			<div style="float:left; margin-top: 15px;">
				<a class="edit" href="javascript:void(0);" onclick="AdminSection.EditErrorCategory();">Edit</a>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	
	<div class="adminListLine" style="height:auto;">
		<div class="adminLabel">
			<strong>Description</strong>
		</div>
		<div class="adminData">
			<?php echo $this -> error -> description ?>
		</div>
	</div>
</div>
