<div class="wrapper">
	<div class="headerPage" style="clear:both; height:30px;">
		<div style="float:left">
			User: <?php echo $this -> user -> getFullName() ?>
		</div>
	</div>
	<div class="breadCrumbs" style="clear:both;">
		<div class="link">Home</div>
		<div class="breadcrumb-arrow"></div>
		<div class="link"><a class="breakcrumbLink" href="<?php echo PATH ?>admin/users?page=1">Users</a></div>
		<div class="breadcrumb-arrow"></div>
		<div class="link">User: <?php echo $this -> user -> getFullName() ?></div>
		<div style="float:right">
			<a class="delete" href="<?php echo PATH. 'admin/delete/' . $this -> user -> _userId; ?>/user">
				Delete
			</a>
		</div>	
	</div>
	<div class="adminListLine" style="height:auto; padding-top: 0px;">
		<div class="adminLabel">
			<strong>Full Name</strong>
		</div>
		<?php echo $this -> user -> getFullName() ?>
	</div>
	<div class="adminListLine" style="height:auto;">
		<div class="adminLabel">
			<strong>Email</strong>
		</div>
		<div class="adminData">
			<a style="color:#2a2a2a" href="mailto:<?php echo $this -> user -> email ?>"><?php echo $this -> user -> email ?></a>
		</div>
	</div>
	<div class="adminListLine" style="height:auto;">
		<div class="adminLabel">
			<strong>Registered</strong>
		</div>
		<div class="adminData">
			<?php echo $this -> recordedTime -> formatDate($this -> user -> startDate, $this -> settingTimeZone) . ' / ' . $this -> recordedTime -> formatTime($this -> user -> starTime, $this -> settingTimeZone) ?>
		</div>
	</div>
	<div class="headerPage" style="border-bottom: 1px solid #d9d9d9; padding: 10px;">
		Credentials
	</div>
	<div class="adminListLine" style="height:auto;">
		<div class="adminLabel">
			<strong>Username</strong>
		</div>
		<div class="adminData">
			<?php echo $this -> user -> userName ?>
		</div>
	</div>
</div>