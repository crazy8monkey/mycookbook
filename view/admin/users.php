
<div class="wrapper">
	<div class="headerPage" style="clear:both; height:30px;">
		<div style="float:left">
			Users	
		</div>
	</div>
	<div class="breadCrumbs" style="clear:both;">
		<div class="link">Home</div>
		<div class="breadcrumb-arrow"></div>
		<div class="link">Users</div>
	</div>
	<?php if(!empty($this->allUserList)) : ?>
	<?php foreach ($this->allUserList as $key => $users) { ?>
	<a class="adminSelect" href="<?php echo PATH; ?>admin/user/<?php echo $users['userId']?>">
		<div class="adminListLine">
			<div style="float:left">
				<?php echo $users['firstName'] . ' ' . $users['lastName']?>	
			</div>
			<div style="float:right">
				<?php echo $this -> recordedTime -> formatDate($users['date'], $this -> settingTimeZone) . ' / ' . $this -> recordedTime -> formatTime($users['time'], $this -> settingTimeZone) ?>				
			</div>
		</div>
	</a>		
	<?php } ?>

	<script>
	//puts pagination element into the corresponding section
	$(document).ready(function(){
		$(".pagination").appendTo(".pagination-section");	
	});
	</script>	
	<div class="pagination-section"></div>
	<?php endif; ?>
	<?php if(empty($this->allUserList)) : ?>
		<div style="padding:10px;">
			Currently there are no users registered.	
		</div>
		
	<?php endif; ?>
</div>
