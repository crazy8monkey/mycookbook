<div class="wrapper">
	<div class="headerPage" style="clear:both; height:30px;">
		<div style="float:left">
			Error Tickets	
		</div>
	</div>
	<div class="breadCrumbs" style="clear:both;">
		<div class="link">Home</div><div class="breadcrumb-arrow"></div><div class="link">Errors</div>	
	</div>
	<?php require "view/errorSubList.php" ?>
	<div style="margin-left:220px;">
		<?php if(!empty($this->errorList)) : ?>
		<?php foreach ($this->errorList as $key => $errors) { ?>
		<a class="adminSelect" href="<?php echo PATH ?>admin/error/<?php echo $errors['id']?>">
			<div class="adminListLine">
				<div style="float:left">
					MCB - <?php echo $errors['id']?>	
				</div>
				<div style="float:right">
					<?php echo $this -> recordedTime -> formatDate($errors['date'], $this -> settingTimeZone) . ' / ' . $this -> recordedTime -> formatTime($errors['time'], $this -> settingTimeZone) ?>
				</div>
			</div>		
		</a>
		<?php } ?>
		
		<div class="pagination-section"></div>
		<?php endif; ?>
	</div>
	<script>
	//puts pagination element into the corresponding section
	$(document).ready(function(){
		$(".pagination").appendTo(".pagination-section");	
	});
	</script>	
	
	
	<?php if(empty($this->errorList)) : ?>
		<div style="margin-left:220px;">
			Currently there are no error tickets submitted.
		</div>
	<?php endif; ?>
</div>
