<div class="login-box">
	<a href="<?php echo PATH ?>">
		<img src="<?php echo PATH ?>public/images/home-logo.png" style="width: 270px; display:table; margin:0 auto 5px auto;"/>
	</a>
	<?php if (isset($this -> adminUserLogin)): ?>
	<div class="login-input-section">
		<div class="padding">
			<div class="loginHeader">
				Login Administration 
			</div>	
		</div>
		<div class="divider"></div>
		<form method="post" id="adminLogin" action="<?php echo PATH ?>login/adminLogin">
			<div class="padding" style="padding-bottom: 5px;">
				<div class="login-label">
					Login
				</div>
				<div style="margin-bottom:10px;" class="input-line">
					<input type="text" style="width: 280px;" name='admin-username' />
				</div>	
			</div>
			<div class="padding" style="padding-top: 0px;">
				<div class="login-label">
					Password
				</div>
				<div class="input-line">
					<input type="password" style="width: 280px;" name='admin-password' />	
				</div>
			</div>
			<div class="divider"></div>			
			<div class="padding">
				<div style="float:left; margin-top: 7px;">
					<a class="forgotCredentials" href="<?php echo PATH ?>login/AdminForgotCredentials">Forgot Credentials?</a>
				</div>
				<div style="float:right">
					<input type="submit" value="Login" />
				</div>
				<div style="clear:both"></div>
			</div>
		</form>

	</div>
	
	
	
	<?php endif; ?>
	<?php if (isset($this -> adminForgotCredentials)): ?>
	<div class="login-input-section">
		<div class="padding">
			<div class="loginHeader">
				Forgot Credentials 
			</div>	
		</div>
		<div class="divider"></div>	
		<form method="post" id="adminForgotCredentials" action="<?php echo PATH ?>login/sendAdminCredentials">
			<div class="padding">
				<div class="login-label">
					Please enter your email.
				</div>	
				<input type="text" style="width:280px;" name='admin-email' />
			</div> 
			<div class="divider"></div>
			<div class="padding">
				<div style="float:left; margin-top:7px;">
					<a class="forgotCredentials" href="<?php echo PATH ?>/login">Back to Login</a>
				</div>
				<div style="float:right">
					<input type="submit" value="Submit" />					
				</div>
				<div style="clear:both;"></div>
			</div>			
		</div>
	</form>
		
	</div>
	
	
	
	<?php endif; ?>
	<?php if (isset($this -> adminCredentialsSent)): ?>
	<div class="login-input-section">
		<div class="padding">
			<div class="loginHeader">
				Forgot Credentials 
			</div>	
		</div>
		<div class="divider"></div>
		<div class="padding">
			<div class="login-label">
				Credentials has been sent.<br /><br />
				Please check your email to retrieve your credentials.
			</div>
		</div>
		<div class="divider"></div>
		<div class="padding" style="padding-top: 20px; padding-bottom: 20px;">
			<a class="backToLogin" href="<?php echo PATH ?>login">Back to Login</a>
		</div>
	</div>
	<?php endif; ?>
</div>
<script>
	$(document).ready(function() {
		$('.error-close').click(function() {
			$('.login-error-message').slideUp('fast');
		});
	}); 
</script>