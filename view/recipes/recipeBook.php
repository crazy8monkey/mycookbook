<div id="singleUserRecipeBook">
	<div class="container-fluid">

		<div class="singleRecipeElement" ng-repeat="recipe in userRecipes" ng-mouseover="showOverlay()" ng-mouseleave="hideOverlay()">
			<div class="recipeImage noImage">
				<div class="mouseOverOverlay" ng-show="showOverlayButtons">
					<div class="view"></div>
					<div class="deleteRecipe"></div>
				</div>
			</div>
			<div class="name">
				{{recipe.recipe_name}}
			</div>
			<div class="category">
				{{getCategoryName(recipe.categoryID)}}
				{{recipeCategoryName}}
			</div>
		</div>
						
		

	</div>
	
	<div class="leftCategoryButtons">
		<div class="categoryList">
			<div class="mui-select">
				<select name='userRecipeCategorySelect' ng-model='userRecipeCategorySelect'>
					<option>All Recipes</option>
					<option ng-repeat='x in userCategories' value='{{x.category_id}}'>{{x.categoryName}}</option>
				</select>			
			</div>
		</div>
		<div class="deleteCategory"></div>
		<div class="editCategory"></div>
		
	</div>
	
	
	
	
	<div class="addCategory">+</div>
	
</div>