<div class="wrapper">
	<div class="subPageMenu">
		<div class="normal-header">
			<h1>Account Settings</h1>	
		</div>
		<a href="javascript:void(0)" id="fullNameHyperLink" onclick="SettingsPage.openFullNameForm()">
			<div id="fullNameLink" class="settingLink subSettingsMenuOn">
				Full name
			</div>	
		</a>
		<a href="javascript:void(0)" onclick="SettingsPage.openEmailUsernameForm()">
			<div id="UserNameLink" class="settingLink">
				Email/Username
			</div>
		</a>
		<a href="javascript:void(0)" onclick="SettingsPage.openChangePasswordForm()">
			<div id="PasswordLink" class="settingLink">
				Change Password
			</div>
		</a>
		<a href="javascript:void(0)" onclick="SettingsPage.openDeleteAccountSection()">
			<div class="settingLink" style="border-bottom:none;">
				Delete Account
			</div>
		</a>
	</div>
	
	<div class="recipe-white-box-section" style="margin-left: 300px;">	
		<div class="normal-header">
			<h1 id="accountSettingsHeader">Full Name</h1>
		</div>
		<div class="white-box-content" style="padding:0px; width:100%;">
			<div class="settings-section">
				<div class='toggle-content' id="fullNameForm" style="display:block">
					<form method="post" id="fullNameFormData" style="display:block" action="<?php echo PATH . 'recipes/fullNameUpdate/'. $this ->profile -> _userId ?>">
						<div class="settings-input-line">
							<div class="settings-inner-label">First Name</div>
							<input class="user-input" type="text" name="user-first-name" style="height: 35px; font-size: 18px;" value="<?php echo $this -> profile -> firstName ?>" />	
						</div>
						<div class="settings-input-line">
							<div class="settings-inner-label">Last Name</div>
							<input class="user-input" type="text" name="user-last-name" style="height: 35px; font-size: 18px;" value="<?php echo $this -> profile -> lastName ?>" />	
						</div>
						<div class="button-holder">
							<input type="submit" value="Submit">
						</div>
					</form>
				</div>
			</div>
			<div class="settings-section">
				<div class='toggle-content' id="credentialsNameForm">
					<form method="post" id="userCredentialsFormData" action="<?php echo PATH . 'recipes/userCredentialsUdate/'. $this -> profile -> _userId ?>">
						<div class="settings-input-line">
							<div class="settings-inner-label">Email</div>
							<input class="user-input" type="text" name="user-email" style="height: 35px; font-size: 18px;" value="<?php echo $this -> profile -> email ?>" />	
						</div>
						<div class="settings-input-line">
							<div class="settings-inner-label">Username</div>
							<input class="user-input" type="text" name="user-username" style="height: 35px; font-size: 18px;" value="<?php echo $this -> profile -> userName ?>" />	
						</div>
						<div class="button-holder">
							<input type="submit" value="Submit">	
						</div>
					</form>
				</div>
			</div>
			<div class="settings-section">
				<div class='toggle-content' id="passwordContent">
					<form method="post" id="passwordFormData"  action="<?php echo PATH . 'recipes/updatePassword/'. $this -> profile -> _userId ?>">
						<div class="settings-input-line">
							<div class="settings-inner-label">New Password</div>
							<input type="password" id="password" name="newPassword" />
						</div>
						<div class="settings-input-line">
							<div class="settings-inner-label">Confirm Password</div>
							<input type="password" name="ConfirmNewPassword" />
						</div>
						<div class="button-holder">
							<input type="submit" value="Submit">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="deleteAccountPopup">
	<div class="popup-header bordertopLeftRadius borderTopRightRadius" style="color:#737373; background: white; border: none; display:block">
		<div style="float:left; padding: 7px 0px 5px 12px; font-size: 24px;">Delete your account</div>
		<a href="javascript:void(0)" onclick="SettingsPage.closeDeleteAccountPopup()"><div class="close-button" style="float:right; border:none;"></div></a>
		<div style="clear:both"></div>
	</div>
	<div style="padding: 0px 0px 5px 12px;">
		<span style="font-family:arial; color:black">Are you sure you want to delete your account? <br />
		This action cannot be undone</span>
		<div style="padding: 14px 0px 13px 0px;">
			<a href="<?php echo PATH ?>recipes/deleteAccount/" class="redLink">
				Delete
			</a>
		</div>
	</div>
</div>

						
<?php 
require_once "libs/Message.php";
$msg = new Message();
				
$msg -> show(); ?>

