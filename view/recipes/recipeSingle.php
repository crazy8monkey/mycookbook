<div id="recipeSinglePage">
	<div class="recipePicture NoPicture"></div>	
	<div id="recipePicture">		
		<div class="photoButtons">
			<div class="UploadButton hidden-xs"></div>	
			<div class="deleteButton hidden-xs"></div>		
			
			<div class="row visible-xs">
				<div class="col-xs-6 UploadButton"></div>
				<div class="col-xs-6 deleteButton"></div>
			</div>
		</div>
	</div>	

<div class='container'>
	<form ng-submit="saveRecipeSubmit()">
		<div class="content">
			<div class="row pageMessage" ng-show="showMessage">
				<div class="col-md-12">
					<div class='loadingSpinner' ng-show="loadingFinal">
						<img ng-src="{{ finishLoading }}" />
					</div>
						
					<div ng-class="{'alert-danger': errorMessageShow, 'alert-success': successMessageShow}" class='text alert'>
						{{errorMessage}}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="formLabel">Recipe Name</div>
						<input type="text" name="recipeName" ng-model="recipeName">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="formLabel">Category</div>
						<div class='mui-select'>
								<select name='recipeCategory' ng-model="recipeCategory">
									<option ng-repeat='x in userRecipeCategories' value='{{x.category_id}}'>{{x.categoryName}}</option>
								</select>
							</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="formLabel">Ingredients</div>
						<textarea name="recipeIngredients" ui-tinymce="tinymceOptions" class="tinyMCE" ng-model="recipeIngredients"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="inputLine">
						<div class="formLabel">Instructions</div>
						<textarea name="recipeInstructions" ui-tinymce="tinymceOptions" class="tinyMCE" ng-model="recipeInstructions"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="submit" class="greenButton" value="SAVE RECIPE" />
				</div>
			</div>
			
			
			
		</div>
		
	</form>

</div>

</div>