<div class="wrapper" style="width:770px">
<script type="text/javascript">
	tinyMCE.init({
		
		selector: "textarea[name='recipeIngredients'], textarea[name='recipeInstructions']",
		menubar : false,
		
		onchange_callback : function(editor) {
			tinyMCE.triggerSave();
			$("#" + editor.id).valid();
		},
		setup: function(ed) {
        	ed.on('init', function() {
        	    this.getDoc().body.style.fontSize = '12px';
    	    });
 	   },
	});
</script>

<div class="recipe-white-box-section" style="margin-bottom: 28px;">
	<div class="normal-header">
		<h1 >Edit Recipe</h1>
	</div>

	<form enctype="multipart/form-data" id="edit-recipe-form" name="add-new-recipe-form" method="post" action="<?php echo PATH ?>recipes/editSave/<?php echo $this -> recipe -> recipeID; ?>">
		<div class="image-hover">
			<?php
			if ($this -> recipe -> recipeImage == 'default-image.png') {
				echo '<div class="defaultImageContainer"><img id="recipeImage" src="' . PATH . 'view/recipes/images/' . Session::User() -> _userId . '/default-image/default-image.png" /></div>';
			} else {
				echo '<img id="recipeImage" src="' . PATH . 'view/recipes/images/' . Session::User() -> _userId . '/' . $this -> recipe -> recipeImage . '" width="100%" style="margin-bottom: -4px;" />';
			}
			?>
			<a class="change-picture-label" href="javascript:void(0);" onclick="RecipeController.openImageUploadDiv()">Change Picture</a>
			<div class="image-upload-section">
				<input type="file" name="recipeImage" id="editRecipeImage" style="border:none;" /><a href="javascript:void(0);" onclick="MainApp.clearFileInput();" class="clearFileInput">x clear</a>
				
				<div style="float:right">
					<span class="required">(.jpg, .jpeg, .png, .gif only)</span>
					
					<a class="closeFileUpload" href= "javascript:void(0);" onclick="RecipeController.clearImageUploadSection();">x close</a>
				</div>	
			</div>
		</div>
		<div class="breadcrumbs">
			<div class="link"><a href="<?php echo PATH ?>recipes">Home</a></div>
			<div class="breadcrumb-divider"></div>
			<div class="link"><a href="<?php echo PATH ?>recipes/viewrecipe?id=<?php echo $this -> recipe -> recipeID; ?>">View Recipe: <?php echo $this -> recipe -> recipeName; ?></a></div>
			<div class="breadcrumb-divider"></div>
			<div class="link"><strong>Edit Recipe: <?php echo $this -> recipe -> recipeName; ?></strong></div>
			<div style="clear:both;"></div>
		</div>
		<div class="form-container" style="margin-top: 8px;">
			<div class="form-row">
				<div class="form-input">
					<div class="form-label" style="margin-top:8px;"><span class="required"><strong>Recipe Name</strong> *</span></div>
					<input type="text" name="recipeName" value="<?php echo $this -> recipe -> recipeName; ?>"/>
				</div>
			</div>
			<div class="form-row">
				<div class="form-input">
					<div class="form-label">Recipe Category</div>
					<select name="recipeCategory">
						<?php foreach ($this->recipeCategories as $key => $value) { ?>
						<option value="<?php echo $value['category_id']?>" <?php if($this -> recipe -> categoryID == $value['category_id']) echo 'selected'; ?>><?php echo $value['categoryName']?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-input">
					<div class="form-label" style="margin-top:8px;"><span class="required"><strong>Recipe Ingredients</strong> *</span></div>
					<div style="margin-left: 170px;">
						<textarea class="required" name="recipeIngredients"><?php echo $this -> recipe -> ingredients; ?></textarea>
					</div>
				</div>
			</div>
			<div class="border-divide"></div>
			<div class="form-row">
				<div class="form-input">
					<div class="form-label" style="margin-top:8px;"><span class="required"><strong>Recipe Instructions</strong> *</span></div>
					<div style="margin-left: 170px;">
						<textarea class="required" name="recipeInstructions"><?php echo $this -> recipe -> instructions; ?></textarea>
					</div>
					<input type="hidden" id="currentRecipeImage" name="currentRecipeImage" value=""/>
					<input type="hidden" id="currentRecipeImageExt" name="currentRecipeImageExt" value=""/>
				</div>
			</div>
		</div>
		<div class="button-holder">
			<input type="submit" name="recipeChanged" value="Submit" />
		</div>	
	</form>
</div>
</div>