<div class="wrapper" style="width:770px">
	<div class="recipe-white-box-section">
		
		<div class="normal-header" style="position:relative">
			<h1><?php echo $this -> recipe -> recipeName; ?></h1>
			<a class="edit-recipe-button" href="<?php   echo PATH . 'recipes/editrecipe?id='. $this -> recipe -> recipeID; ?>"/>
				<div class="edit-button">
					<div>Edit</div>
				</div>
			</a>
		</div>
		<div class="image-hover">
			<div id="currentImageLabel">
				<?php
					$category = Category::WithID($this -> recipe -> categoryID);
				?>
				
				Category: <?php echo $category -> categoryName; ?>
			</div>
		
		
		
	<?php
	if($this -> recipe -> recipeImage == 'default-image.png') {
		echo '<div class="defaultImageContainer"><img id="recipeImage" src="'. PATH . 'view/recipes/images/' . Session::User() -> _userId .'/default-image/default-image.png"  /></div>';
	} else {
		echo '<img id="recipeImage" src="'. PATH . 'view/recipes/images/' . Session::User() -> _userId . '/' . $this -> recipe -> recipeImage. '" width="100%" style="margin-bottom: -4px;"/>';	
	}	
	
	?>
	
	</div>
	<div class="breadcrumbs">
		<div class="link"><a href="<?php echo PATH ?>recipes">Home</a></div>
		<div class="breadcrumb-divider"></div>
		<div class="link"><strong>View Recipe: <?php echo $this -> recipe -> recipeName; ?></strong></div>
		<div style="clear:both;"></div>
	</div>
	<div class="white-box-content" style="padding-bottom: 6px; width: 754px;">
		
		<div class="ingredients" style="padding-top: 0px;">
			<h1 style="color:black; margin-bottom:5px;">Ingredients</h1>
			<p><?php echo html_entity_decode($this -> recipe -> ingredients); ?></p>
		</div>
		<div class="border-divide"></div>
			<div class="instructions">
				<h1 style="color:black; margin-bottom:5px;">Instructions</h1>
				<p><?php echo html_entity_decode($this -> recipe -> instructions); ?></p>
			</div>
		</div>
	</div>
</div>	
<?php 
require_once "libs/Message.php";
$msg = new Message();
				
$msg -> show(); ?>
<script>
	$('.messages.change').delay(500).fadeOut(2500);
</script>	

