<div class="greyBackgroundContent" id="yourProfilePage">
	<div class='container-fluid' id="profilePicContent">
		
		
		
		
		<div class="profilePic">
			<div class="image NoImage"></div>
		</div>
		
		<div class="photoButtons">
			<div class="UploadButton hidden-xs"></div>	
			<div class="deleteButton hidden-xs"></div>		
			
			<div class="row visible-xs">
				<div class="col-xs-6 UploadButton"></div>
				<div class="col-xs-6 deleteButton"></div>
			</div>
		
		</div>
		
		
		
	</div>	
</div>
<div class='container'>
	<form ng-submit="saveAccountSubmit()">
		<div class="content">
			<div class="row pageMessage" ng-show="showMessage">
				<div class="col-md-12">
					<div class='loadingSpinner' ng-show="loadingFinal">
						<img ng-src="{{ finishLoading }}" />
					</div>
						
					<div ng-class="{'alert-danger': errorMessageShow, 'alert-success': successMessageShow}" class='text alert'>
						{{errorMessage}}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 FormHeader">
					Your Profile
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine">
						<div class="formLabel">First Name</div>
						<input type="text" name="settingsFirstName" ng-model="settingsFirstName">
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine">
						<div class="formLabel">Last Name</div>
						<input type="text" name="settingsLastName" ng-model="settingsLastName">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine">
						<div class="formLabel">Email</div>
						<input type="text" name="settingsEmail" ng-model="settingsEmail">
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine">
						<div class="formLabel">Username</div>
						<input type="text" name="settingsUsername" ng-model="settingsUsername">
					</div>
				</div>
			</div>
			
		</div>
		<div class="content">
			<div class="row">
				<div class="col-md-12 FormHeader">
					Change Your Password
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="inputLine">
						<div class="formLabel">New Password</div>
						<input type="password" name="settingsNewPassword" ng-model="settingsNewPassword">
					</div>
				</div>
				<div class="col-md-6">
					<div class="inputLine">
						<div class="formLabel">Confirm Password</div>
						<input type="password" name="settingsConfirmPassword" ng-model="settingsConfirmPassword">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="submit" class="greenButton" value="SAVE ACCOUNT" />
				</div>
			</div>
			
		</div>
		
	</form>

</div>

