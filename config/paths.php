<?php

define ('PATH', 'http://localhost:8888/mycookbook/');
define('LIBS', 'libs/');
define('CLASSES', 'classes/');
define('LISTS', 'lists/');


//image path for recipe upload image
define ('IMAGES_PATH', 'view/recipes/images/');

// This is for other hash keys... Not sure yet
define('HASH_GENERAL_KEY', 'sha256');

// This is for database passwords only
define('HASH_PASSWORD_KEY', 'IAmHungryFeedMe');

