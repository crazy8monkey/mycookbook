<?php


// email header types
define ('MYSQL_ERROR_TYPE', 'MYSQL ERROR');
define ('SHIPPING_ERROR_TYPE', 'CART SHIPPING RATE ERROR');
define ('STRIPE_CHECKOUT_ERROR_TYPE', 'STRIPE CHECKOUT ERROR');
define ('TRACK_ERROR_TYPE', 'TRACKING ERROR');
define ('TAXJAR_API_TYPE', 'TAXJAR API ERROR');


//message types
define ('MYSQL_INSERT_LABEL', 'MySql Insert Error: ');
define ('MYSQL_SELECT_LABEL', 'MySql Select Error: ');
define ('MYSQL_UPDATE_LABEL', 'MySql Update Error: ');
define ('MYSQL_DELETE_LABEL', 'MySql Delete Error: ');
define ('MYSQL_GENERAL_LABEL', 'MySql General Error: ');

//system default error message
define ('SYSTEM_ERROR_MESSAGE', 'Something went wrong with our system. Please try again');


