<?php

class WebsiteErrorList extends BaseObjectList {
	

	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function getErrorCategories() {
		return json_encode($this -> db -> select('SELECT * FROM ErrorReportsCategories'));
	}

}