<?php

class RecipeList extends BaseObjectList {
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	public function getRecipes() {
		return json_encode($this -> db -> select('SELECT * FROM recipes'));
	}
	
	public function getUserRecipes($id) {
		return $this -> db -> select('SELECT * FROM recipes WHERE userID = ' . $id);	
	}

}