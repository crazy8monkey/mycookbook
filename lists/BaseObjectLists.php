<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:58 PM
 */

class BaseObjectList {
    function __construct()
    {    	
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

    public function __sleep()
    {
        return array();
    }

    public function __wakeup()
    {
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

}