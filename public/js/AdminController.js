var AdminController = function() {
	function startAdminLoginPage() {
		$("#adminLogin").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				success: function(data) {
					if(data.redirect) {
						window.location = data.redirect
					}
					if(data.adminErrorLogin) {
						Globals.loadAjaxFlashMessage(data.adminErrorLogin);
						//$("form .error-input-row:first-child input[type='text']").focus();
					}
				}
			})
		});
	}
	
	function startForgotCredentials() {
		$("#adminForgotCredentials").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				success: function(data) {
					
					if(data.errorEmail) {
						Globals.loadAjaxFlashMessage(data.errorEmail, false, "form input[type='text']");
					}
					if(data.verifyEmail) {
						Globals.loadAjaxFlashMessage(data.verifyEmail, false, "form input[type='text']");
					}
					if(data.emptyEmail) {
						Globals.loadAjaxFlashMessage(data.emptyEmail, false, "form input[type='text']");
					}
					if(data.redirect) {
						window.location = data.redirect
					}
					
				}
			})
		});
	}
	
	function startCategoryPage() {
		if($(".adminListLineLink").length > 0) {
			$("#noCategoryText").hide();
		} else {
			$("#noCategoryText").show();
		}
						
		$("#newCategory").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				success :function(data) {
					if(data.emptyCategory) {
						Globals.loadAjaxFlashMessage(data.emptyCategory, false, "form input[type='text']");
					}
					if(data.duplicateCategory) {
						Globals.loadAjaxFlashMessage(data.duplicateCategory, false, "form input[type='text']");
					}
					if(data.success) {
						var userData = $("form input[type='text']").val();
						Globals.loadAjaxFlashMessage(data.success, "form input[type='text']");
						$(".new-category-form").hide();
						$("#categoryLists").append("<a class='adminListLineLink' href='javascript:void(0);' rel='" +  data.id + "'><div class='adminListLine'>" + userData + "</div></a>")
										   .children(':last')
										   .hide().fadeIn(1000);
						$("#noCategoryText").hide();						
					}
				},
			});					
		});
		
	
    
	    //##### Delete record when delete Button is clicked #########
	    $(document).on("click", "#categoryLists .adminListLineLink", function(e) {
	        e.preventDefault();
	        var myData = $(this).attr('rel'); //build a post data structure
	        //alert(myData);
	        
	        $.ajax({
	            type: "POST", 
	            url: "delete/" + myData + "/category", 
	            dataType:"json", 
	            data:myData, //post variables
	            success:function(data){
	          		//on success, hide element user wants to delete.
	           		$('a[rel="'+ myData + '"]').fadeOut("500").remove();
	           		Globals.loadAjaxFlashMessage(data.categoryDeleted);
	            	$("#noCategoryText").show();
	            	
	            	  if($(".adminListLineLink").length > 0) {
							$("#noCategoryText").hide();
						} else {
							$("#noCategoryText").show();
						}

	            },
	        });
	      
	    });
	}
	
	function startErrorTicketPage() {
		$("#categoryChange").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			var optionSelected = $("#ticketCategory option:selected" ).val();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				success :function(data) {
					if(data.categoryChanged) {
						Globals.loadAjaxFlashMessage(data.categoryChanged);
						$(".editErrorCategory").hide();
						$("a.edit").show();
						$("span#errorTypeChanged").hide().html(optionSelected).fadeIn('3000');
					}
				},
			});					
		});
	}
	
	return {
		startAdminLoginPage: function() {
			startAdminLoginPage();
		},
		startForgotCredentials: function() {
			startForgotCredentials();
		},
		toggleNewCategoryForm: function() {
			$(".new-category-form").toggle();
			$("form input[type='text']").val('');
		},
		EditErrorCategory: function() {
			$(".editErrorCategory").show();
			$("a.edit").hide();
		},
		closeEditCategory: function() {
			$(".editErrorCategory").hide();
			$("a.edit").show();
		},
		startCategoryPage: function() {
			startCategoryPage();	
		},
		startErrorTicketPage: function() {
			startErrorTicketPage();	
		}
	}
}();
