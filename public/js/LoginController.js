var LoginController = function() {
	
	var myCookBookApp = Globals.AngularVariable(); 
	
	var loginForm = Globals.currentAppPath() + 'Login/userNameLogin';
	var signUpForm = Globals.currentAppPath() + 'Login/signUpForm';
	var credentialsForm = Globals.currentAppPath() + 'Login/credentials';	
	var loadingSpinnerFinish = Globals.currentAppPath() + 'public/images/ajax-loader.gif';
		
	function Initialize() {
		
		
		myCookBookApp.config(function($routeProvider) {
			$routeProvider
			.when('/login', {
				controller : "loginController",
                templateUrl : 'view/login/login.php',
                title: "MyCookBook: Login",
                PageTitleHeader: "Login",
                loginControllerButtons: true,
                homeLink: Globals.currentAppPath(),
                normalPage: true,
                showPageTitleHeader: true,
                loginController: true
            }).when('/signup', {
            	controller : "signUpController",
                templateUrl : 'view/login/signup.php',
                title: "MyCookBook: Signup",
                PageTitleHeader: "Sign up",
                loginControllerButtons: true,
                homeLink: Globals.currentAppPath(),
                normalPage: true,
                showPageTitleHeader: true,
                loginController: true
            }).when('/credentials', {
            	controller : "credentialsController",
                templateUrl : 'view/login/credentials.php',
                title: "Retrieve Login Credentials",
                PageTitleHeader: "Retrieve Login Credentials",
                loginControllerButtons: true,
                homeLink: Globals.currentAppPath(),
                normalPage: true,
                showPageTitleHeader: true,
                loginController: true
			}).when('/resetPassword', {
            	controller : "passwordResetController",
                templateUrl : 'view/login/passwordReset.php',
                title: "Reset Your Password",
                PageTitleHeader: "Reset Password",
                loginControllerButtons: true,
                homeLink: Globals.currentAppPath(),
                normalPage: true,
                showPageTitleHeader: true,
                loginController: true
			}).otherwise({redirectTo: '/login'});
		});
		
		
		//login page
		myCookBookApp.controller('loginController', function($scope, $sce, $http) {
			var loginFormPost;
			$scope.loginControllerButtons = true;
			$http.get(loginForm).success(function(response) {
				$scope.finishLoading = loadingSpinnerFinish;
				
				
				$scope.showPage = true;
				loginFormPost = response.userLoginPostURL;
				
				
				var userNameLogin = '';
				var passwordLogin = '';
				
				//submit the form
				$scope.loginFormSubmit = function() {
					$scope.loadingFinal = true;
					
					
					if($scope.userNameLogin !== undefined) {
						userNameLogin = $scope.userNameLogin;
					}
					
					if($scope.passwordLogin !== undefined) {
						passwordLogin = $scope.passwordLogin;
					}
					
					//not the most effienct way to send the data via ajax
					//this is temporary, just to make it work, clean this up later
					$http({
						method  : 'POST',
						url     : loginFormPost,
						data 	: "userNameLogin=" + userNameLogin + 
								  "&passwordLogin=" + passwordLogin,
						headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
					})
					.success(function(data) {
						
						if(data.loginErrorMessage) {
							$scope.errorMessage = data.loginErrorMessage;
							$scope.showMessage = true;	
						}
						
						if(data.redirect) {
							Globals.redirect(data.redirect);
						}
						
					}).finally(function() {
						// "complete" code here
    					$scope.loadingFinal = false;
					});
					
					
				};
			});		
			
		});
		
		//signup page
		myCookBookApp.controller('signUpController', function($scope, $sce, $http) {
			$scope.loginControllerButtons = true;
			$http.get(signUpForm).success(function(response) {
				var firstNameData = '';
				var lastNameData = '';
				var userNameData = '';
				var emailData = '';
				var passworData = '';
				var verifyPassworData = '';		
				
				var signupFormURL;		
				
				$scope.showPage = true;
				$scope.finishLoading = loadingSpinnerFinish;
				
				$scope.signupFormSubmit = function() {
					signupFormURL = response.signupFormUrl;
					$scope.loadingFinal = true;
					$scope.firstNameFocus = false;
					
					
					
					if($scope.firstName !== undefined) {
						firstNameData = $scope.firstName;
					}
					
					if($scope.lastName !== undefined) {
						lastNameData = $scope.lastName;
					}
					
					if($scope.userName !== undefined) {
						userNameData = $scope.userName;
					}
					
					if($scope.email !== undefined) {
						emailData = $scope.email;
					}
					
					if($scope.password !== undefined) {
						passworData = $scope.password;
					}
					
					if($scope.confirmPassword !== undefined) {
						verifyPassworData = $scope.confirmPassword;
					}
					
					$http({
						method  : 'POST',
						url     : signupFormURL,
						data 	: "firstName=" + firstNameData +
								  "&lastName=" + lastNameData +
								  "&userName=" + userNameData +
								  "&email=" + emailData +
								  "&password=" + passworData +
								  "&confirmPassword=" + verifyPassworData,
						headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
					}).success(function(data) {
						
						if(data.firstName) {
							$scope.errorMessage = data.firstName;
							$scope.showMessage = true;
						}
						
						if(data.firstNameNoLetters) {
							$scope.errorMessage = data.firstNameNoLetters;
							$scope.showMessage = true;
							$scope.firstName = '';
							$scope.firstNameFocus = true;
						}
						
						if(data.lastName) {
							$scope.errorMessage = data.lastName;
							$scope.showMessage = true;
						}
						
						if(data.lastNameNoLetters) {
							$scope.errorMessage = data.lastNameNoLetters;
							$scope.showMessage = true;
						}
						
						if(data.userName) {
							$scope.errorMessage = data.userName;
							$scope.showMessage = true;
						}
						
						if(data.userNameNoWhiteSpace) {
							$scope.errorMessage = data.userNameNoWhiteSpace;
							$scope.showMessage = true;						
						}
					
						if(data.Email) {
							$scope.errorMessage = data.Email;
							$scope.showMessage = true;
						}
						
						if(data.verifyEmail) {
							$scope.errorMessage = data.verifyEmail;
							$scope.showMessage = true;
						}
						
						if(data.duplicateEmail) {
							$scope.errorMessage = data.duplicateEmail;
							$scope.showMessage = true;
						}
						
						if(data.password) {
							$scope.errorMessage = data.password;
							$scope.showMessage = true;
						} 
						
						if(data.passwordNoWhiteSpace) {
							$scope.errorMessage = data.passwordNoWhiteSpace;
							$scope.showMessage = true;
						}
						
						if(data.passwordOnlyLettersNumbers) {
							$scope.errorMessage = data.passwordOnlyLettersNumbers;
							$scope.showMessage = true;
						}
						
						if(data.minPasswordLength) {
							$scope.errorMessage = data.minPasswordLength;
							$scope.showMessage = true;
						}
						
						if(data.confirmPasswordCheck) {
							$scope.errorMessage = data.confirmPasswordCheck;
							$scope.showMessage = true;
						}
						
						if(data.duplicateUserName) {
							$scope.errorMessage = data.duplicateUserName;
							$scope.showMessage = true;
						}
						
						if(data.redirect) {
							Globals.redirect(data.redirect);
						}
					
					}).finally(function() {
						// "complete" code here
    					$scope.loadingFinal = false;
					});
					
					
				};
				
			});
			
		});
		
		//retrieve credentials page
		myCookBookApp.controller('credentialsController', function($scope, $sce, $http, $location) {
			var credentialFormPost;
			$scope.loginControllerButtons = true;
			$http.get(credentialsForm).success(function(response) {
				//$scope.credentialsEmail = $sce.trustAsHtml(response.userEmail);
				$scope.showPage = true;
				credentialFormPost = response.credentialFormURL;
				$scope.finishLoading = loadingSpinnerFinish;
				
				var userEmail = '';

				$scope.credentialsFormSubmit = function() {
					
					$scope.loadingFinal = true;
					
					if($scope.userEmail !== undefined) {
						userEmail = $scope.userEmail;
					}
					
					//not the most effienct way to send the data via ajax
					//this is temporary, just to make it work, clean this up later
					$http({
						method  : 'POST',
						url     : credentialFormPost,
						data 	: "userEmail=" + userEmail,
						headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
					})
					.success(function(data) {
						
						  if(data.emptyEmail) {
						  	$scope.errorMessage = data.emptyEmail;
						  	$scope.showMessage = true;
						  	
						  } 
						  if(data.verifyEmail) {
						  	$scope.errorMessage = data.verifyEmail;
						  	$scope.userEmail = '';
						  	$scope.showMessage = true;
						  	
						  }
						  if(data.errorEmail) {
						  	$scope.errorMessage = data.errorEmail;
						  	$scope.showMessage = true
						  }
						  
						  if(data.validEmail) {
						  	$scope.hideForm = true;
						  	
						  }
						
					}).finally(function() {
						// "complete" code here
    					$scope.loadingFinal = false;
					});
					
					
				};
				
			});
			
			$scope.loginClick = function() {
				$location.path('/login');
			}
			
		});
		
		//password reset page
		myCookBookApp.controller('passwordResetController', function($scope, $sce, $http, $location) {
			$scope.showPage = true;
			
		});
		
		
		
		myCookBookApp.run(['$location', '$rootScope', function($location, $rootScope) {
			$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
				$rootScope.title = current.$$route.title;
				$rootScope.PageTitleHeader = current.$$route.PageTitleHeader;
				$rootScope.loginControllerButtons = current.$$route.loginControllerButtons;
				$rootScope.homeLink = current.$$route.homeLink;
				$rootScope.normalPage = current.$$route.normalPage;
				$rootScope.showPageTitleHeader = current.$$route.showPageTitleHeader;
				$rootScope.loginController = current.$$route.loginController;
			});
		}]);
			    
	    
	}
	
	return {
		Initialize: Initialize
	}
}();

