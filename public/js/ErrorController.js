var ErrorController = function() {
	
	var myCookBookApp = Globals.AngularVariable();
	var errorReportForm = Globals.currentAppPath() + 'error/errorSubmitForm';
	var errorCategories = Globals.currentAppPath() + 'Error/getErrorCategories';
	var loadingSpinnerFinish = Globals.currentAppPath() + 'public/images/ajax-loader.gif';
	
	function getQueryVariable(variable) {
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
	}
	
	
	function Initialize() {
	
		
		myCookBookApp.controller('errorSuggestionsPage', function($scope, $sce, $http, $location) {
			
			$scope.categories = [$scope.categories];
			$scope.showPageTitleHeader = true;
			$scope.loginController = true;
			$scope.recipeController = false;
			var errorLoginSubmitForm;
			
			$scope.tinymceOptions =  {
			    selector:'textarea',
				height: 100,
				plugins: "autoresize",

				menu: {
					    
				}
			}
			
			
			$http.get(errorReportForm).success(function(response) {
				
				$scope.categories = JSON.parse([response.errorCategories]);
				$scope.errorType = $scope.categories[0].id;
				
				errorLoginSubmitForm = response.errorLoginForm;
				$scope.finishLoading = loadingSpinnerFinish;
				$scope.SessionContent = response.CurrentSession;
				
				//$scope.categories[0].value = 0
				
				if(response.CurrentSession == true) {
					$scope.homeLink = response.recipesLink;
					$scope.recipeMainAppButtons = true;
					$scope.fullPage = true
					
					if (getQueryVariable('suggestions')) {
						$scope.title = "Error/Suggestions";
						$scope.PageTitleHeader = "Error/Suggestions"
						
					} else {
						$scope.title = "404 Oops! Broken Link";
						$scope.PageTitleHeader = "404: Page does not exists";
							
					}
					
				} else {
					$scope.homeLink = Globals.currentAppPath();
					$scope.loginControllerButtons = true;
					$scope.normalPage = true;
				}
			    

				

				
				
				
				var fullName = '';
				var email = '';
				var errorType = '';
				var Description = '';
				
				console.log($scope.formData);
				$scope.errorFormSubmit = function() {
					$scope.loadingFinal = true;
					
					if($scope.errorFullName !== undefined) {
						fullName = $scope.errorFullName;
					}
					
					if($scope.errorEmail !== undefined) {
						email = $scope.errorEmail;
					}
					
					
					
					if($scope.errorDescription !== undefined) {
						Description = $scope.errorDescription;
					}
					
					$http({
						  method  : 'post',
						  url     : errorLoginSubmitForm,
						  data 	: "errorFullName=" + fullName + 
								  "&errorEmail=" + email +
								  "&errorType=" + $scope.errorType +
								  "&errorDescription=" + Description,
						  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
					})
					.success(function(data) {
						  console.log($scope.errorType);
						if(data.firstName) {
						  	$scope.errorMessage = data.firstName;
						  	$scope.showMessage = true;
						  	$scope.errorMessageShow = true;
						}
						
						if(data.firstNameOnlyLetters) {
							$scope.errorMessage = data.firstNameOnlyLetters;
						  	$scope.showMessage = true;
						  	$scope.errorMessageShow = true;
						}
						
						if(data.emptyEmail) {
							$scope.errorMessage = data.emptyEmail;
						  	$scope.showMessage = true;
						  	$scope.errorMessageShow = true;
						}
						
						if(data.wrongEmailFormat) {
							$scope.errorMessage = data.wrongEmailFormat;
						  	$scope.showMessage = true;
						  	$scope.errorMessageShow = true;
						}
						
						if(data.errorType) {
							$scope.errorMessage = data.errorType;
						  	$scope.showMessage = true;
						  	$scope.errorMessageShow = true;
						}
						
						if(data.errorDescription) {
							$scope.errorMessage = data.errorDescription;
						  	$scope.showMessage = true;
						  	$scope.errorMessageShow = true;
						}
						
						if(data.finishedForm) {
							$scope.errorMessage = data.finishedForm;
							$scope.showMessage = true;
							$scope.successMessageShow = true;  
							$scope.errorMessageShow = false;	
							$scope.errorDescription = '';
						}
						
						
						
					}).finally(function() {
						// "complete" code here
    					$scope.loadingFinal = false;
					});
					
					
				};
				
				
			});
			
		});
		

		
		
		
	}
	
	
	function TinyMCE() {
		tinymce.init({ 
			selector:'textarea',
			height: 100,
			plugins: "autoresize",
			menu: {
			    
			  }
		});
	}
	
	return  {
		Initialize: Initialize,
		TinyMCE: TinyMCE
	}
}();
