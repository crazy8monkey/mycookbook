var RecipeController = function() {
	
	var myCookBookApp = Globals.AngularVariable(); 
	var recipeMainAppPage = Globals.currentAppPath() + 'recipes/startRecipeAppPage';
	var LoggedInAccountSettings = Globals.currentAppPath() + 'recipes/LoggedinUserSettings';
	var recipeSinglePage = Globals.currentAppPath() + 'recipes/recipeSinglePage';
	var userRecipeBook = Globals.currentAppPath() + 'recipes/LoggedInUserRecipeBook';
	
	var loadingSpinnerFinish = Globals.currentAppPath() + 'public/images/ajax-loader.gif';
	
	
	function StartRecipeForm() {
		//http://stackoverflow.com/questions/8903854/check-image-width-and-height-on-upload-with-javascript
		var _URL = window.URL || window.webkitURL;
		
		var errorMessage = "<div class='message error'>" +
								"Image needs to be at least 768px wide." +
							"</div>"
							
		var errorMessageFileExt = "<div class='message error'>" +
								  	  "jpg, jpeg, png, gif file formats are allowed." +
								  "</div>"
		
		$("#image-upload").change(function(e) {
			//validating image width
		    var file, img;
		    if ((file = this.files[0])) {
		        img = new Image();
				img.onload = function () {
					if(this.width < 768) {
						Globals.loadAjaxFlashMessage(errorMessage, "#image-upload");
						//$("#image-upload").val('');
						$("a.clearFileInput").hide();
					} 
				}	
				img.src = _URL.createObjectURL(file);
		
		    }
			//validate file extension
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				Globals.loadAjaxFlashMessage(errorMessageFileExt, "#image-upload")
    			$("a.clearFileInput").hide();
			} 
			
			if($("input[type=file]").val() != "") {
				$("a.clearFileInput").show();
			} 
		});					
	}
	
	
	
	
	
	function clearFileInput() {
		$("input[type=file]").val('');
		$("a.clearFileInput").hide();
	}

	
	function startEditRecipeForm() {
		var _URL = window.URL || window.webkitURL;
		
		var errorMessage = "<div class='message error'>" +
								"Image needs to be at least 768px wide." +
							"</div>"
							
		var errorMessageFileExt = "<div class='message error'>" +
								  	  "Only jpg, jpeg, png, gif file formats are allowed." +
								  "</div>"
								  
		
		
		
		$("#editRecipeImage").change(function(event) {
			//validating image width
		    var file, img;
		    if ((file = this.files[0])) {
		        img = new Image();
				img.onload = function () {
					//alert(img.width);
					if(this.width < 768) {
						Globals.loadAjaxFlashMessage(errorMessage, "#editRecipeImage");
						$("a.clearFileInput").hide();
					}
				}	
				img.src = _URL.createObjectURL(file);
		    }
			//validate file extension
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    			Globals.loadAjaxFlashMessage(errorMessageFileExt, "#editRecipeImage");
    			$("a.clearFileInput").hide();
			} 
			
			
			
			if($("input[type=file]").val() != "") {
				$("a.clearFileInput").show();
				
			} else {
				$('.image-hover').mouseleave(function() {
					$('.image-upload-section').fadeOut("500");
				});	
			} 
		});
	}
	
	function openImageUploadDiv() {
		$('.image-upload-section').show();
	}
	
	function openNewCategoryForm() {
		$("#NewCategory").show();
		$("#NewCategory form").fadeIn();
		$(".addCategory").addClass("addCategoryOpen");
			
		$("#EditCategory").hide();
		$("#EditCategory form").hide();
		$(".categoryLine .edit").removeClass("editFormOpen");
	}
	
	function closeNewCategoryForm() {
		$("#NewCategory").hide();
		$("#NewCategory form").hide();
		$(".addCategory").removeClass("addCategoryOpen");
		$("#NewCategory form input[type='text']").val('');
	}
	
	function openEditCategoryForm() {
		$("#EditCategory").show();
		$("#EditCategory form").fadeIn();
			
		$("#NewCategory").hide();
		$("#NewCategory form").hide();
		$(".categoryLine .edit").addClass("editFormOpen");
		$(".addCategory").removeClass("addCategoryOpen");
	}
	
	function closeEditCategoryForm() {
		$("#EditCategory").hide();
		$("#EditCategory form").hide();
		$(".categoryLine .edit").removeClass("editFormOpen");
		$(".edit").removeClass("editFormOpen");
	}
	
	function hideDeleteEditButtons() {
		$(".categoryLine .edit").hide();
		$(".categoryLine .delete").hide();
	}
	
	
	function currentCategory() {
		var currentCategorySelected = $("select#recipeCategories option:selected").text();
		var urlPath = window.location.pathname.split("/");
		if (urlPath[urlPath.length - 1] == "recipes") {
			return "All Recipes";
		}
		else {
			return urlPath[urlPath.length - 1];	
		}
	}
	
	function selectCategory() {
		$("select#recipeCategories option[value='"+ currentCategory() + "']").attr('selected','selected');
   		$("#EditCategory form input[type='text']").val(currentCategory());
	}
	
	function removeCurrentCategoryFormAttr() {
		$("a#deleteCategory").removeAttr("href");		
		$("#EditCategory").hide();
		$(".edit").removeClass("editFormOpen");
		$("form#deleteCategory").removeAttr("action");
		$("#EditCategory form").removeAttr("action");
	}
	
	function urlPATH() {
		return window.location.pathname;
	}
	
	function ifWindowContains(text) {
		return window.location.href.indexOf(text)
	}
	
	function loadCurrentCategory() {
		if(ifWindowContains(currentCategory()) > 0) {
   			if(currentCategory() !== "Uncategorized") {
   				selectCategory();
   				loadCurrentCategoryFormAction();
				$("#currentCategorySelected").val(currentCategory());
			}
    	} else if(ifWindowContains("recipes") > 0) {
    		selectCategory()
    		hideDeleteEditButtons();
    	}
		if(currentCategory() === "Uncategorized") {
			selectCategory()
			hideDeleteEditButtons();
		}
		$("#recipeCategories").change(function(){
			var formURL = $("#currentCategoryForm").attr("action");
			var postData = $("#currentCategoryForm").serialize();
		
			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				success :function(data) {
					if(urlPATH()!=window.location){
    					window.history.pushState({path: data.redirect},'',data.redirect);
    					$("select#recipeCategories").attr('disabled','disabled');
    					$(".recipes-container").fadeOut(500, function() {
    						$.get(data.redirect, function( data ) {
  								//puts mysql data into .recipes-container element
  								$(".recipes-container").hide().html($(data).find(".recipe-white-box-section"));
  								//loading edit category form
  								loadEditCategoryForm();
  								$("select#recipeCategories").delay(1000).removeAttr('disabled');
							});
							$(".recipes-container").delay(300).fadeIn(700);
    					});
    				}
				}
			});	
			
			//loads category text live time		
			$("span#currentSelectedCategory").html($(this).val())
			 
			closeEditCategoryForm();
			closeNewCategoryForm();
			
			
			if($("span#currentSelectedCategory").html() === "Uncategorized") {
				hideDeleteEditButtons();
				removeCurrentCategoryFormAttr();
			} else if ($("span#currentSelectedCategory").html() === "All Recipes"){
				removeCurrentCategoryFormAttr();
				hideDeleteEditButtons();
			} else {
				loadCurrentCategoryFormAction();
				$(".categoryLine .edit").fadeIn();
				$(".categoryLine .delete").fadeIn();
			}
			
			return false;
		});
		
		
		//this is needed so when the user refreshes the browser
		//it selects the current category selected
		$("span#currentSelectedCategory").html(currentCategory());
		
	}
	

	
	function Initialize() {
		
		myCookBookApp.config(function($routeProvider) {
			$routeProvider
			.when('/', {
				controller : "recipeMainController",
                templateUrl : 'view/recipes/main.php',
                title: "MyCookBook",
                PageTitleHeader: "Login",
                loginControllerButtons: false,
                recipeMainAppButtons: true,
                homeLink: Globals.currentAppPath() + "recipes#/",
                blanketShow: true,
                fullPage: true,
                loginController: false,
                showPageTitleHeader: false,
            }).when('/add-recipe', {
				controller : "addRecipeController",
                templateUrl : 'view/recipes/recipeSingle.php',
                title: "Add New Recipe",
                PageTitleHeader: "Login",
                loginControllerButtons: false,
                recipeMainAppButtons: true,
                homeLink: Globals.currentAppPath() + "recipes#/",
                blanketShow: false,
                fullPage: true,
                showPageTitleHeader: true,
                loginController: false,
                recipeController:true
            }).when('/account/settings', {
				controller : "accountController",
                templateUrl : 'view/recipes/profile.php',
                title: "Your Account",
                PageTitleHeader: "Login",
                loginControllerButtons: false,
                recipeMainAppButtons: true,
                homeLink: Globals.currentAppPath() + "recipes#/",
                blanketShow: false,
                fullPage: true,
                loginController: false,
                showPageTitleHeader: false,
            }).when('/my-recipe-book', {
				controller : "myRecipeBookController",
                templateUrl : 'view/recipes/recipeBook.php',
                title: "Your Recipe Book",
                PageTitleHeader: "Login",
                loginControllerButtons: false,
                recipeMainAppButtons: true,
                homeLink: Globals.currentAppPath() + "recipes#/",
                blanketShow: true,
                fullPage: true,
                loginController: false,
                showPageTitleHeader: false,
            }).otherwise({redirectTo: '/'});
		});
		
		
		
		myCookBookApp.controller('recipeMainController', function($scope, $sce, $http) {
			
			
		});
		
		myCookBookApp.controller('addRecipeController', function($scope, $sce, $http) {
			var saveRecipeURL;
			
			$scope.tinymceOptions =  {
				    selector:'textarea',
					height: 100,
					plugins: "autoresize",
	
					//menu: {
						    
					//}
				}
			
			$http.get(recipeSinglePage).success(function(response) {
				$scope.userRecipeCategories = response.userRecipeCategories;
				$scope.recipeCategory = $scope.userRecipeCategories[0].category_id;
				
				saveRecipeURL = response.saveRecipeURL;
				
				var recipeName = '';
				var recipeCateogry = '';
				var recipeIngredients = '';
				var recipeInstructions = '';
				
				
				
				$scope.finishLoading = loadingSpinnerFinish;
				
				$scope.saveRecipeSubmit = function() {
					$scope.loadingFinal = true;
					
					if($scope.recipeName !== undefined) {
						recipeName = $scope.recipeName;
					}
					
					if($scope.recipeCategory !== undefined) {
						recipeCateogry = $scope.recipeCategory;
					}
					
					if($scope.recipeIngredients !== undefined) {
						recipeIngredients = $scope.recipeIngredients;
					}
					
					if($scope.recipeInstructions !== undefined) {
						recipeInstructions = $scope.recipeInstructions;
					}
					
					
					$http({
						method  : 'POST',
						url     : saveRecipeURL,
						data 	: "recipeName=" + recipeName + 
								  "&recipeCategory=" + recipeCateogry +
								  "&recipeIngredients=" + recipeIngredients +
								  "&recipeInstructions=" + recipeInstructions,
						headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
					})
					.success(function(data) {
						
						if(data.emptyRecipeName) {
							$scope.errorMessage = data.emptyRecipeName;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;	
						}
						
						if(data.emptyRecipeIngredients) {
							$scope.errorMessage = data.emptyRecipeIngredients;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.emptyRecipeInstructions) {
							$scope.errorMessage = data.emptyRecipeInstructions;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.SaveError) {
							$scope.errorMessage = data.SaveError;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						
					}).finally(function() {
						// "complete" code here
    					$scope.loadingFinal = false;
					});
					
					
				}
				
				//$scope.homeLink = response.recipesLink;
			});
			
		});
		
		myCookBookApp.controller('accountController', function($scope, $sce, $http) {
			var saveAccountURL;
			$http.get(LoggedInAccountSettings).success(function(response) {
				//var userSettings = JSON.parse([response.userSettings])
				//console.log(response.userSettings]));
				
				$scope.settingsFirstName = response.userSettings.firstName;
				$scope.settingsLastName = response.userSettings.lastName;
				$scope.settingsEmail = response.userSettings.email;
				$scope.settingsUsername = response.userSettings.userName;
				
				saveAccountURL = response.saveAccountURL;
				
				var firstName = '';
				var lastName = '';
				var userName = '';
				var userEmail = '';
				var newPassword = '';
				var newPasswordConfirm = '';
				
				$scope.finishLoading = loadingSpinnerFinish;
				
				$scope.saveAccountSubmit = function() {
					$scope.loadingFinal = true;
					
					if($scope.settingsFirstName !== undefined) {
						firstName = $scope.settingsFirstName;
					}
					
					if($scope.settingsLastName !== undefined) {
						lastName = $scope.settingsLastName;
					}
					
					if($scope.settingsUsername !== undefined) {
						userName = $scope.settingsUsername;
					}
					
					if($scope.settingsEmail !== undefined) {
						userEmail = $scope.settingsEmail;
					}
					
					if($scope.settingsNewPassword !== undefined) {
						newPassword = $scope.settingsNewPassword;
					}
					
					if($scope.settingsConfirmPassword !== undefined) {
						newPasswordConfirm = $scope.settingsConfirmPassword;
					}
					
					
					//not the most effienct way to send the data via ajax
					//this is temporary, just to make it work, clean this up later
					$http({
						method  : 'POST',
						url     : saveAccountURL,
						data 	: "settingsFirstName=" + firstName + 
								  "&settingsLastName=" + lastName +
								  "&settingsEmail=" + userEmail +
								  "&settingsUsername=" + userName +
								  
								  "&settingsNewPassword=" + newPassword +
								  "&settingsConfirmPassword=" + newPasswordConfirm,
						headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
					})
					.success(function(data) {
						
						if(data.emptyFirstName) {
							$scope.errorMessage = data.emptyFirstName;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;	
						}
						
						if(data.onlyLettersFirstName) {
							$scope.errorMessage = data.onlyLettersFirstName;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.emptyLastName) {
							$scope.errorMessage = data.emptyLastName;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.onlyLettersLastName) {
							$scope.errorMessage = data.onlyLettersLastName;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.emptyEmailMessage) {
							$scope.errorMessage = data.emptyEmailMessage;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.veryifyUserEmail) {
							$scope.errorMessage = data.veryifyUserEmail;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.emptyUserNameMessage) {
							$scope.errorMessage = data.emptyUserNameMessage;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.userNameNoWhiteSpace) {
							$scope.errorMessage = data.userNameNoWhiteSpace;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.passwordNoWhiteSpace) {
							$scope.errorMessage = data.passwordNoWhiteSpace;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.minPasswordLength) {
							$scope.errorMessage = data.minPasswordLength;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.confirmPasswordCheck) {
							$scope.errorMessage = data.confirmPasswordCheck;
							$scope.errorMessageShow = true;
							$scope.showMessage = true;
						}
						
						if(data.AccountUpdated) {
							$scope.errorMessage = data.AccountUpdated;
							$scope.errorMessageShow = false;
							$scope.successMessageShow = true;
							$scope.showMessage = true;
						}
						
					}).finally(function() {
						// "complete" code here
    					$scope.loadingFinal = false;
					});
					
				}
				

			});
		});
		
		//my recipe book
		myCookBookApp.controller('myRecipeBookController', function($scope, $sce, $http) {
			$http.get(userRecipeBook).success(function(response) {
				//$scope.homeLink = response.recipesLink;
				$scope.userRecipes = response.userRecipes;
				$scope.userCategories = response.userCategories;
				
				
				
				$scope.userRecipeCategorySelect = $scope.userCategories[0].category_id;
				
				
				$scope.getCategoryName = function(id){
			    	 angular.forEach(response.userCategories, function(value, key) {
  						if(value.category_id === id) {
  							$scope.recipeCategoryName = value.categoryName;
  						}
					});
			    };
			
				$scope.showOverlay = function() {
					this.showOverlayButtons = true;
				}    
				
				$scope.hideOverlay = function() {
					this.showOverlayButtons = false;
				}
			   
				
				
				
			});
		});
		
		
		myCookBookApp.run(['$location', '$rootScope', function($location, $rootScope) {
			$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
				$rootScope.title = current.$$route.title;
				$rootScope.PageTitleHeader = current.$$route.PageTitleHeader;
				$rootScope.loginControllerButtons = current.$$route.loginControllerButtons;
				$rootScope.recipeMainAppButtons = current.$$route.recipeMainAppButtons;
				$rootScope.homeLink = current.$$route.homeLink;
				$rootScope.blanketShow = current.$$route.blanketShow;
				$rootScope.fullPage = current.$$route.fullPage;
				$rootScope.showPageTitleHeader = current.$$route.showPageTitleHeader;
				$rootScope.loginController = current.$$route.loginController;
				$rootScope.recipeController = current.$$route.recipeController;
			});
		}]);
		
		

	}
	
	return {
		StartAddRecipeForm: function() {
			StartRecipeForm();
		},
		StartEditRecipeForm: function() {
			startEditRecipeForm();
			$('.image-hover').mouseenter(function() {
				$('a.change-picture-label').show();
			}).mouseleave(function() {
				$('a.change-picture-label').hide();
			});
			
			var ImageName = $("#recipeImage").attr("src");

			var lastImageNamePathArray = ImageName.split("/").pop();
			
			var fileExt = lastImageNamePathArray.split(".")[1];
			
			$("#currentRecipeImage").val(lastImageNamePathArray);
			$("#currentRecipeImageExt").val(fileExt);
		},
		clearFileInput: function() {
			clearFileInput();
		},
		clearImageUploadSection: function() {
			$('.image-upload-section').hide();
		},
		openImageUploadDiv: function() {
			openImageUploadDiv();
		},
		openNewCategoryForm: function() {
			openNewCategoryForm();
		},
		closeNewCategoryForm: function() {
			closeNewCategoryForm();
		},
		OpenEditCategoryForm: function() {
			openEditCategoryForm();
		},
		closeEditCategoryForm: function() {
			closeEditCategoryForm();
		},
		Initialize: Initialize
	}
	
}();
