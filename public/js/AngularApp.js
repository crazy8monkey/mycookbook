var AngularAppStart = function(){
		var myCookBookApp = Globals.AngularVariable();
		
		function Initialize() {
			//change page title
			//http://hzhou.me/2014/08/07/AngularJS-Change-Page-Title-Based-on-Routers-Dynamically/
			myCookBookApp.run(['$location', '$rootScope', function($location, $rootScope) {
				$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
					$rootScope.title = current.$$route.title;
					$rootScope.PageTitleHeader = current.$$route.PageTitleHeader;
				});
			}]);
		}	
	return {
		Initialize: Initialize
	}
}()



