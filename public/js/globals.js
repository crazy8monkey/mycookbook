var Globals = function() {
	
	var currentAppPath = 'http://localhost:8888/mycookbook/';
	var myCookBookApp;
	
	function closeMessaging() {
		$('.message').fadeOut('fast');
	}
	function hideFormOverlay() {
		$(".form-overlay").fadeOut("fast");
	}
	
	
	
	return {
		closeFlashMessage: function() {
			closeMessaging()
		},
		
		closePopups:function(formElements, textBoxInput) {
			$(".form-overlay").click(function() {
				hideFormOverlay();
				if(formElements) {
					$(formElements).fadeOut("fast");	
				}
				if(textBoxInput) {
					$(textBoxInput).val('')
				}
			});
		},
		
		loadAjaxFlashMessage: function(data, clearInput, focusInput) {
			//http://stackoverflow.com/questions/183638/making-my-ajax-updated-div-fade-in
			$('.message-holder').hide().html(data).fadeIn('fast');
			$(".message").delay(1500).fadeOut(3500);
			if (clearInput) {
				$(clearInput).val('');
			}
			
			if(focusInput) {
				$(focusInput).focus();
			}
			
		},
		showPageFlashMessage: function(element, text, focusInput, clearInput) {
			$(element + " .loadingSpinner").hide();
	  		$(element + " .text").addClass('alert alert-danger');
	  		$(element + " .text").hide().html(text).fadeIn('fast');
	  		
	  		if(focusInput) {
				$(focusInput).focus();
			}
			
			if (clearInput) {
				$(focusInput).val('');
			}
		},
		closeFlashMessageFade: function () {
			$(".message").delay(1000).fadeOut(3500);
		},
		toggleDropDown: function() {
			$(".dropDownMenu").toggle();
			$(".dropDownArrow").toggleClass("dropDownArrowClicked");			
		},
		openErrorSuggestions: function() {
			$(".errorSuggestionBox").fadeIn("fast");
			$(".form-overlay").fadeIn("fast");
			$(".dropDownArrow").removeClass("dropDownArrowClicked");	
			$(".dropDownMenu").hide();
		},
		closeErrorSuggestions: function() {
			$(".errorSuggestionBox").fadeOut("fast");
			$(".form-overlay").fadeOut("fast");
		},
		ajaxPost: function(form, responses) {
			$(form).submit(function(e){
				e.preventDefault();
				
				
				var formURL = $(this).attr("action");
				var postData = $(this).serialize();
				
			});
		},
		AngularVariable: function() {
			return window.angular.module('myCookBookApp', ['ngRoute', 'ngAnimate', 'ui.tinymce']);
		},
		currentAppPath: function() {
			return currentAppPath;
		},
		mobileDropDown: function() {
			$(document).click(function (e) {
                if ($(e.target).closest('header .dropDownElement .bars, header .dropDownElement .dropDown').length === 0) {
                    $("header .dropDownElement .dropDown").hide();
                    $("header .dropDownElement .bars").removeClass('selected');
                }
            });
			
			$("header .dropDownElement .dropDown").toggle();
			$("header .dropDownElement .bars").toggleClass('selected');
		},
		redirect: function(string) {
			return window.location.assign(string)
		}
	}	
	
}();
