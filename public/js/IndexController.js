var IndexController = function() {
	
	function CloseFormPopup() {
		$(".form-overlay").fadeOut("fast");
		//if login form is showing
		if($(".login-form").length==1) {
			$(".login-form").fadeOut("fast");	
			$('form').find("input[type=text], input[type=password]").val("");
			
		}
		//if signup form is showing
		if($(".signup-form").length==1) {
			$(".signup-form").fadeOut("fast");
			$('form').find("input[type=text], input[type=password]").val("");
		}
	}
	
	function validateNewUserForm() {
	
		$("#new-user-form").validate({
			rules : {
				passwordStartRecipeBook : {
					required: true,
					minlength: 6
				},
				confirmPassword : {
					required: true,
					minlength: 6,
					equalTo : "#password"
				},
				emailStartRecipeBook: {
      				required: true,
      				email: true
    			}
			},
			messages : {
				firstNameStartRecipeBook : {
					required : "First Name Required"
				},
				lastNameStartRecipeBook : {
					required : "Last Name Required"
				},
				emailStartRecipeBook : {
					required : "Email Required"
				},
				usernameStartRecipeBook : {
					required : "Username Required"
				},
				passwordStartRecipeBook : {
					required : "Please Provide a Password",
					minlength: "Must be 6+ characters long"
				},
				confirmPassword : {
					required: "Please Provide a Password",
					minlength: "Must be 6+ characters long",
					equalTo: "Passwords to not match"
				}
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				error.insertBefore(element);
			}
		});
	}
	
	function openLoginForm() {
		$(".form-overlay").fadeIn("fast");
		$(".login-form").fadeIn("fast");
	}

	function openSignUpForm() {
		$(".form-overlay").fadeIn("fast");
		$(".signup-form").fadeIn("fast");
	}
	
	function formOverlayClick() {
		$(".form-overlay").click(function(){
			if($(".signup-form").length == 1) {
				$(".signup-form").fadeOut('fast');
				$(".form-overlay").fadeOut("fast");
			}
			if($(".login-form").length == 1) {
				$(".login-form").fadeOut('fast');
				$(".form-overlay").fadeOut("fast");
			}
		});
	}
	
	return {
		StartDocument: function() {
			validateNewUserForm();
			formOverlayClick();
		},
		CloseFormPopup: function() {
			CloseFormPopup()
		},
		openLoginForm: function() {
			openLoginForm()
		},
		openSignUpForm: function() {
			openSignUpForm()
		}
	}
}();










