<?php

class Admin extends Controller {
		
	public function __construct() {
		parent::__construct();
		Session::init();
		
		$this -> view -> adminHeader = "";
		$this -> view -> adminHome = "";
		$loggedIn = Session::get('adminLoggedIn');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage('login');
			exit();
		}
	}	
	
	private function setAccountTimeZone() {
		$this -> view -> settingTimeZone = $this -> model -> timeZone;
	}
	
	public function AdminForgotCredentials() {
		$this -> view -> adminHeader = "";
		$this -> view -> adminForgotCredentials = "";
		$this -> view -> css = array("admin/admin-style.css", "messages.css", "admin/adminLogin.css");
		$this -> view -> js = array(PATH. "public/js/admin-section.js");
		$this -> view -> startJsFunction = array('AdminSection.startForgotCredentials();');
		$this -> view -> render('admin/index');
	}
	
	public function sendAdminCredentials() {
		$AdminemailInput = $_POST['admin-email'];
		$this -> model -> sendAdminCredentials($AdminemailInput);
	}
	public function AdminCredentialsSent() {
		$this -> view -> adminHeader = "";
		$this -> view -> adminCredentialsSent = "";
		$this -> view -> css = array("admin/admin-style.css", "admin/adminLogin.css");
		$this -> view -> render('admin/index');
	}
	
	
	public function users() {
		$this -> view -> title = "Users: Administration";
		$this -> view -> allUserList = $this -> model -> allUserList();
		$this -> setAccountTimeZone();
		$this -> view -> css = array("admin/admin-style.css", "admin/home-admin-style.css", "admin/usersPage.css", "messages.css");
		$this -> view -> render('admin/users');
	}
	
	public function user($id) {
		$this -> view -> title = "User Single: Administration";
		$this -> view -> user = User::WithID($id);
		$this -> setAccountTimeZone();
		$this -> view -> css = array("admin/admin-style.css", "admin/usersPage.css");
		$this -> view -> render("admin/user-single");
	}
	
	public function errors() {
		
		$this -> view -> title = "Errors: Administration";
		$this -> setAccountTimeZone();
		$this -> view ->errorList = $this -> model -> errorList();
		$this -> view -> css = array("admin/admin-style.css", "admin/error-admin-style.css", "admin/errorSinglePage.css", "messages.css");
		$this -> view -> render("admin/errors");
	}

	public function error($id) {
		$this -> view -> title = "Error Ticket: Administration";
		$this -> view -> error = WebsiteError::WithID($id);
		$this -> setAccountTimeZone();
		$this -> view -> categories = $this -> model -> errorCategoryList();
		$this -> view -> js = array(PATH. "public/js/AdminController.js");
		$this -> view -> startJsFunction = array('AdminController.startErrorTicketPage();');
		$this -> view -> css = array("admin/admin-style.css", "admin/errorSinglePage.css", "messages.css");
		$this -> view -> render("admin/error-single");
	}
	public function errorCatoryEdit($id) {
		$category = array();
		$category['id'] = $id;
		$category['ticketCategory'] = $_POST['ticketCategory'];
		
		$this -> model -> errorCatoryEdit($category);
	}
	
	public function errorCategory() {
		$this -> view -> title = "Categories: Administration";
		$this -> setAccountTimeZone();
		$this -> view -> css = array("admin/admin-style.css", "admin/error-admin-style.css", "admin/categories.css", "messages.css");
		$this -> view -> js = array(PATH. "public/js/AdminController.js");
		$this -> view -> startJsFunction = array('AdminController.startCategoryPage();');
		$this -> view -> errorCategoryList = $this -> model -> errorCategoryList();
		$this -> view -> render("admin/category");
	}
	
	
	public function adminLogin() {
		$user = New User();
		$user -> userName = $_POST['admin-username'];
		$user -> password = $_POST['admin-password'];
		
		$user -> userLogin(false, true);
			
		
		//$this -> model -> adminLogin();
	}
	
	
	public function delete($id, $type) {
		switch ($type) {
			case "errors";
				$error = WebsiteError::WithID($id);
				$error -> delete();
				break;
			case "category";
				$category = WebsiteError::WithCategoryID($id);
				$category -> deleteCategory();
				break;
			case "user";
				$user = User::WithID($id);
				$user -> deleteAccount(true);
				break;
		}
	}
	
	public function save($type, $id = false) {
		switch ($type) {
			case "errorCategory";
				$errorCategory = new WebsiteError();
				$errorCategory -> errorType = $_POST['newCategory'];
				if($errorCategory -> validateErrorCategory()) {
					$errorCategory -> AddCategory();					
				}
				break;
			
		}
	}
	
	public function adminLogout() {
		Session::destroy();
		$this -> redirect -> redirectPage('login');
		exit();
	}

}
