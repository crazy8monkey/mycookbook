<?php
class Error extends Controller {
	
	private $loggedIn; 
	
	
	public function __construct() {
		Session::init();
		//calls the Controller class
		parent::__construct();
		$this -> loggedIn = Session::get('loggedIn');
		
	}
	
	private function StylesSheets() {
		return array("style.css", 'ErrorController.css');
	}

	public function index() {
		$this -> view -> headerTitle = "";
		$this -> view -> angular = " ng-controller='errorSuggestionsPage'";		
		$this -> view -> css = $this -> StylesSheets();
		$this -> view -> js = array(PATH . "public/js/ErrorController.js");
		$this -> view -> AngularMethod = array('ErrorController.Initialize()');		
		$this -> view -> render('error/index');

	}
	
	
	public function errorSubmitForm() {
		$error = new WebsiteErrorList();
		
		$this -> json -> multipleJSONOjbects(array('errorLoginForm' => $this -> pages -> submitErrorPostBack(),
												   'errorCategories' => $error -> getErrorCategories(),
												   "CurrentSession" => $this -> loggedIn,
												   "recipesLink" => $this -> pages -> recipeMainApp()));
	}
	
	//insert error
	public function insertError() {
		require_once "model/error_model.php";
		$errorModel = new Error_Model();
		$errorInsert = New WebsiteError();
		
		if($this -> loggedIn == 0) {
			$errorInsert -> sessionLoggedIn = false;
			$errorInsert -> name = $_POST['errorFullName'];
			$errorInsert -> email = $_POST['errorEmail'];
		} else if ($this -> loggedIn == 1) {
			$errorInsert -> sessionLoggedIn = true;
			$errorInsert -> userIDNumber = Session::User() -> _userId;
		}
		$errorInsert -> errorType = $_POST['errorType'];	
		$errorInsert -> description = $_POST['errorDescription'];
		if($errorInsert -> validate()) {
			$errorInsert -> save();
		}
	}

}
?>