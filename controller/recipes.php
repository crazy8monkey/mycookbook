<?php


class Recipes extends Controller {

	private $loggedInUser;
	
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('loggedIn');
		Session::get('user');
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage("home");
			exit();
		}
		$this -> loggedInUser = Session::User() -> _userId;
		$this -> view -> js = array(PATH . "public/js/RecipeController.js");
	}
	
	private function StylesSheets() {
		return array("style.css", 'RecipeController.css');
	}
	
	public function index() {	
		$this -> view -> angular = "";
		$this -> view -> bodyClass = "ng-class='{recipeMain: blanketShow}' ";
		$this -> view -> css = $this -> StylesSheets();
		$this -> view -> AngularMethod = array('RecipeController.Initialize();');		
		$this -> view -> render('recipes/recipeController');
	}
	
	public function startRecipeAppPage() {
		//$error = new WebsiteErrorList();		
		$this -> json -> multipleJSONOjbects(array("recipesLink" => $this -> pages -> recipeMainApp()));
	}
	
	//profile page
	public function LoggedinUserSettings() {
		$this -> json -> multipleJSONOjbects(array("userSettings" => User::WithID($this -> loggedInUser),
												   'saveAccountURL' => $this -> pages -> Save('account'),
												   'saveProfilePic' => $this -> pages -> Save('profilePicture')));
	}
	
	//recipeSinglePage
	public function recipeSinglePage() {
		$userCategories = New CategoryList();
		
		$this -> json -> multipleJSONOjbects(array("userRecipeCategories" => $userCategories -> getUserCategories($this -> loggedInUser),
												   'saveRecipeURL' => $this -> pages -> Save('recipe')));
	}

	public function LoggedInUserRecipeBook() {
		$recipeList = New RecipeList();
		$category = New CategoryList();
		
		$this -> json -> multipleJSONOjbects(array("userRecipes" => $recipeList -> getUserRecipes($this -> loggedInUser),
												"userCategories" =>$category -> getUserCategories($this -> loggedInUser)));
	}
	
	
	//save
	public function save($type) {
		switch ($type) {
			case "account";
				$user = User::WithID($this -> loggedInUser);
				$user -> firstName = $_POST['settingsFirstName'];
				$user -> lastName = $_POST['settingsLastName'];
				$user -> userName = $_POST['settingsUsername'];
				$user -> email = $_POST['settingsEmail'];
				$user -> password = $_POST['settingsNewPassword'];
				$user -> confimPassword = $_POST['settingsConfirmPassword'];
				$user -> SetPassword($_POST['settingsNewPassword']);
				
				if($user -> validateAccount()) {
					$user -> SaveCredentials();
				}
				
				break;
			case "profilePicture";
				$user = User::WithID($this -> loggedInUser);
				break;
			case "recipe";
				$recipe = New Recipe();
				$recipe -> recipeName = $_POST['recipeName'];
				$recipe -> url = preg_replace('#[^a-z.0-9]#i', '-', $_POST['recipeName']);
				$recipe -> category = $_POST['recipeCategory'];
				$recipe -> ingredients = $_POST['recipeIngredients'];
				$recipe -> instructions = $_POST['recipeInstructions'];
				
				if($recipe -> validateRecipe()) {
					$recipe -> Save();	
				}
			
				break;
		}
		
	}
	
	
	public function delete($type, $id) {
		
	}
	
	public function category($category) {
		
		//needs the start session for the flash error message
		Session::init();
		$this -> view -> css = array("main-app.css", "recipe-form.css", "style.css", 'messages.css');
		$this -> view -> links = "";
		$this -> view -> title = "Your Recipe list";
		$this -> view -> recipeList = $this -> model -> recipeCategorySelected($category);
		
		$this -> view -> startJsFunction = array('RecipeController.startRecipeAppPage();');
		$this -> view -> changeImageDirectory = "../../view/recipes/images/" . Session::User() -> _userId;
		$this -> view -> render('recipes/index');
	}
	
	public function changeCategory() {
		$category = New Category();
		$category -> categoryID = $_POST['recipeCategories'];
		$category -> changeCategory();
	}
	

	
	
	//delete recipe
	//public function delete($id) {
	//	$recipe = Recipe::WithID($id);
	//	$recipe -> deleteRecipe();
	//}
	
	

	//category section
	public function newCategory() {
		$category = New Category();
		$category -> categoryName = $_POST['newCategory'];
		
		if($category -> validateCategory()) {
			$category -> Save();
		}
	}

	//delete category
	public function deleteCategory($id) {
		$category = Category::WithID($id);	
		$category -> deleteCategory();
	}
	
	//edit category
	public function editCategory($id) {
		$category = Category::WithID($id);	
		$category -> categoryName = $_POST['editCategory'];
		
		if($category -> validateCategory()) {
			$category -> Save();
		}
	}
	

	public function deleteAccount() {
		$user = New User();
		$user -> deleteAccount();
	}


	public function logout() 
	{
		Session::destroy();
		$this -> redirect -> redirectPage("home");
		exit();
	}

}
