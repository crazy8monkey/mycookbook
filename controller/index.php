<?php


class Index extends Controller {
	public function __construct() {
		//calls the Controller class
		parent::__construct();
		$loggedIn = Session::get('loggedIn');
		if($loggedIn == true) {
			$this -> redirect -> redirectPage('recipes');
		}
		
		$this -> view -> homePageLink = "";
		$this -> view -> css = array("home-page.css", "style.css", 'IndexController.css');
		$this -> view -> js = array(PATH . "public/js/IndexController.js");
		$this -> view -> startJsFunction = array('IndexController.StartDocument();');
	}

	public function index() {
		//calls the view class, and
		//the render function
		$this -> view -> loginController = "";
		$this -> view -> title = "Welcome to MyCookBook";
		$this -> view -> render('index/index');
	}
	
	

}
?>