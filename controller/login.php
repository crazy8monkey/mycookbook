<?php

Class Login Extends Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	private function StylesSheets() {
		return array("style.css", 'LoginController.css');
	}
	
	public function index() {
		$this -> view -> angular = "";
		$this -> view -> css = $this -> StylesSheets();
		$this -> view -> AngularMethod = array('LoginController.Initialize();');
		$this -> view -> js = array(PATH. "public/js/LoginController.js");
		$this -> view -> render('login/loginController');
	}
	
	
	public function passwordReset() {
		Session::init();
		$this -> view -> js = array(PATH . "public/js/LoginController.js");
		$this -> view -> title = "MyCookBook: Reset Password";
		if (isset($_GET['token']) && isset($_GET['userID']))  {
			$this -> view -> userInfo = User::WithID($_GET['userID']);		
			$this -> view -> startJsFunction = array('LoginController.resetPassword();');				
		}
		$this -> view -> css = array("style.css", "messages.css","login/login.css");
		$this -> view -> errorHeader = "";
		$this -> view -> render('login/passwordReset');
		
	}
	
	//inputs
	
	//login form
	public function userNameLogin() {
		$this -> json -> multipleJSONOjbects(array('userLoginPostURL' => $this -> pages -> userLogin()));
		
	}
	
	//sign up form
	public function signUpForm() {
		$this -> json -> multipleJSONOjbects(array("signupFormUrl" => $this -> pages -> signupUserPostBack()));
	}

	//forgot credentials form
	public function credentials() {
		$this -> json -> multipleJSONOjbects(array("credentialFormURL" => $this -> pages -> sendCredentialsPostBack()));
	}
	
	//form posts
	
	//retrieve credentials form
	public function sendCredentials() {
		$user = new User();
		$user -> userType = 0;
		$user -> email = $_POST['userEmail'];
        
       	if($user -> validateEmail()) {
       	    $user -> SendEmail();
       	}
	}
	
	//reset password form
	public function resetPasswordForm() {
		$user = User::WithID($_POST['hiddenUserID']);
		$user -> password = $_POST['newPassword'];
        $user -> SetPassword($_POST['newPassword']);
		
		if($user -> validatePassword()) {
			$user -> SaveNewPassword();
		}	
	}

	//login form
	public function login() {
		$user = New User();
		$user -> userName = $_POST['userNameLogin'];
		$user -> password = $_POST['passwordLogin'];
		
		$user -> userLogin();
	}

	//new user form
	public function newUser() {
		$user = New User();
		$user -> firstName = $_POST['firstName'];
		$user -> lastName = $_POST['lastName'];
		$user -> userName = $_POST['userName'];
		$user -> email = $_POST['email'];
		$user -> password = $_POST['password'];
		$user -> confimPassword = $_POST['confirmPassword'];
		$user -> SetPassword($_POST['password']);
				
		if($user -> validateNewUser()) {
			$user -> newUser();
		}		

	}
		
	public function test() {
		Session::init();
		unset($_SESSION['token']);	
		unset($_SESSION['userID']);
		$this -> view -> css = array("style.css", "messages.css","login/login.css");
		$this -> view -> errorHeader = "";
		$this -> view -> render('login/test');
		//unset($_SESSION['token']); 
	}
	
}
