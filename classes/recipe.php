<?php

class Recipe extends BaseObject {
	
	public $userID;
	public $recipeID;
	public $categoryID;
	public $url;
	public $recipeName;
	public $recipeImage;
	public $currentRecipeImage;
	public $ingredients;
	public $instructions;
	public $category;
	

    public function __sleep() {
        parent::__sleep();	
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function UserID($userID){
        $instance = new self();
        $instance -> userID = $userID;
        $instance -> loadByUserId();
        return $instance;
    }
	
	public static function WithID($recipeID){
        $instance = new self();
        $instance -> recipeID = $recipeID;
        $instance -> loadByRecipeId();
        return $instance;
    }
	
	 protected function loadByRecipeId() {
        $sth = $this -> db -> prepare('SELECT * FROM recipes WHERE id = :id');
        $sth->execute(array(':id' => $this -> recipeID));
        $record = $sth -> fetch();
        $this -> fillRecipeID($record);
    }
	 
	protected function fillRecipeID(array $row){
		$this -> recipeName = $row['recipe_name'];
    	$this -> recipeImage = $row['recipe_image'];
		$this -> currentRecipeImage = $row['recipe_image'];
		$this -> ingredients = $row['ingredients'];
		$this -> categoryID = $row['categoryID'];
		$this -> instructions = $row['instructions'];
    }
	
    protected function loadByUserId() {
        $sth = $this -> db -> prepare('SELECT * FROM recipes WHERE userID = :userID');
        $sth->execute(array(':userID' => $this -> userID));
        $record = $sth -> fetch();
        $this -> fillUserID($record);
    }
	
	protected function fillUserID(array $row){
    	$this -> recipeImage = $row['recipe_image'];
    }
	
	public function validateRecipe() {
		if($this -> validate -> emptyInput($this -> recipeName)) {
			$this -> json -> outputJqueryJSONObject('emptyRecipeName', $this -> msg -> isRequired("Recipe Name"));
			return false;
		} else if($this -> validate -> emptyInput($this -> ingredients)) {
			$this -> json -> outputJqueryJSONObject('emptyRecipeIngredients', $this -> msg -> isRequired("Recipe Ingredients"));
			return false;
		} else if($this -> validate -> emptyInput($this -> instructions)) {
			$this -> json -> outputJqueryJSONObject('emptyRecipeInstructions', $this -> msg -> isRequired("Recipe Instructions"));
			return false;
		}
		return true;
	}
	

	
	public function Save() {
		try {
			$userID = Session::User() -> _userId;
			$userImagePath = 'view/recipes/images/' . $userID . '/';
			
			//edit is not working????
			if(isset($this -> recipeID)) {
					//post data for sending to database
				$postData = array('recipe_url' => $this -> url, 
								  'recipe_name' => htmlentities(stripcslashes($this -> recipeName), ENT_QUOTES),
								  'ingredients' => $this -> validate -> SpecialCharacters($this -> ingredients),  
								  'instructions' => $this -> validate -> SpecialCharacters($this -> instructions),
								  'categoryID' => $this -> category);
							
				$where = array('id' => $this -> recipeID);
				
				$this -> db -> update('recipes', $postData, $where);
								
			} else {
								
				
				
				$this -> db -> insert('recipes', array('recipe_url' => $this -> url, 
									       'recipe_name' => htmlentities(stripcslashes($this -> recipeName), ENT_QUOTES), 
									       'ingredients' => $this -> validate -> SpecialCharacters($this -> ingredients), 
									       'instructions' => $this -> validate -> SpecialCharacters($this -> instructions),
									       'categoryID' => $this ->  category,
										   'userID' => $userID));
										   
										   
				
			}
			
		} catch (Exception $e) {
			
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Recipe Save/edit error: " . $e->getMessage();
			$TrackError -> type = "Recipe SAVE/EDIT";
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('SaveError', SYSTEM_ERROR_MESSAGE);
		}
		
		

	}
	
	public function deleteRecipe() {
		try {
			$userID = Session::User() -> _userId;
			
			if($this -> recipeImage != "default-image.png") {
				//calls the detelImage functions		
				Image::deleteImage('view/recipes/images/' . $userID . '/' . $this -> recipeImage);			
			}
			
			$sth = $this -> db -> prepare("DELETE FROM recipes WHERE id = :id");
			$sth -> execute(array(':id' => $this -> recipeID));
			
			$this -> msg->set($this -> msg->flashMessage('message error', $this -> msg -> recipeDeleted()));
			
			$this -> redirect -> redirectPage("recipes");
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "Recipe Delete error: " . $e->getMessage();
			$TrackError -> type = "Recipe DELETE";
			$TrackError -> SendMessage();
			
			if(isset($this -> recipeID)) {
				$this -> redirect -> redirectPage($this -> pages -> RecipeSingle($this -> recipeID));
			} else {
				$this -> redirect -> redirectPage("addrecipe");
			}
		}
		
	}
	


	
    

}