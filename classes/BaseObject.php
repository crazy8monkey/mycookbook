<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:58 PM
 */

class BaseObject {
    function __construct()
    {    	
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
        $this -> redirect = new Redirect();
		$this -> currentTime = new Time();
		$this -> pages = new Pages();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

    public function __sleep()
    {
        return array();
    }

    public function __wakeup()
    {
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
        $this -> redirect = new Redirect();
		$this -> currentTime = new Time();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

}