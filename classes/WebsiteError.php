<?php

class WebsiteError extends BaseObject {
	
	public $errorID;
	public $categoryID;
	public $name;
	public $email;
	public $description;
	public $errorType;
	public $sessionLoggedIn;	
	public $userIDNumber;
	public $enteredDate;
	public $enteredTime;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	private function getSessionStatus() {
		return $this -> sessionLoggedIn;
	}

    public static function WithID($errorID){
        $instance = new self();
        $instance -> errorID = $errorID;
        $instance -> loadById();
        return $instance;
    }
	
	public static function WithCategoryID($categoryID){
        $instance = new self();
        $instance -> categoryID = $categoryID;
        
        return $instance;
    }
	
	
    protected function loadByID() {
        $sth = $this -> db -> prepare('SELECT * FROM ErrorReports WHERE id = :id');
        $sth->execute(array(':id' => $this->errorID));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	

	
    protected function fill(array $row){
		$this -> errorID = $row['id'];
		$this -> name = $row['name'];
		$this -> email = $row['userEmail'];
		$this -> errorType = $row['errorType'];
		$this -> description = $row['errorDescription'];
		$this -> enteredDate = $row['date'];
		$this -> enteredTime = $row['time'];
    }
	
	
    
    
    public function validate() {
    	if($this -> getSessionStatus() == false) {
    		if ($this -> validate -> emptyInput($this -> name)) {
				$this -> json -> outputJqueryJSONObject('firstName', $this -> msg -> isRequired("First Name"));
				return false;
			} else if($this -> validate -> notUsingLetters($this -> name)) {
				$this -> json -> outputJqueryJSONObject('firstNameOnlyLetters', $this -> msg -> onlyLetters());
				return false;
			} else if ($this -> validate -> emptyInput($this -> email)) {
				$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg -> isRequired("Email"));
				return false;
			} else if($this -> validate -> correctEmailFormat($this -> email)) {
				$this -> json -> outputJqueryJSONObject('wrongEmailFormat', $this -> msg -> emailFormatRequiredMessage());
				return false;
			} else if ($this -> validate -> matchingText($this -> errorType, "0")) {
				$this -> json -> outputJqueryJSONObject('errorType', $this -> msg -> isRequired("Error Type"));
				return false;			
			} else if ($this -> validate -> emptyInput($this -> description)) {
				$this -> json -> outputJqueryJSONObject('errorDescription', $this -> msg -> isRequired("Error description"));
				return false;
			}
		} else {
			if ($this -> errorType == "0") {
				$this -> json -> outputJqueryJSONObject('errorType', $this -> msg -> isRequired("Error Type"));
				return false;
			} else if ($this -> validate -> emptyInput($this -> description)) {
				$this -> json -> outputJqueryJSONObject('errorDescription', $this -> msg -> isRequired("Error description"));
				return false;
			}
		}
		
		return true;
		
		
    }

	public function save() {
		$fullName = "";
		$email = "";
		
		if($this -> getSessionStatus() == false) {
			$fullName = $this -> name;	
			$email = $this -> email;
		} else {
			//echo $this -> userIDNumber;
			$user = User::WithID($this -> userIDNumber);
			$fullName = $user -> getFullName();
			$email = $user -> email;
		}
		
		$this -> db -> insert('ErrorReports', array('name' => $fullName, 
							       					'userEmail' => $email, 
							       					'errorType' => $this -> errorType, 
							       					'errorDescription' => $this -> validate -> DecodeHTMLCharacters($this -> description),
								   					'date' => date("Y-m-d", $this -> currentTime -> getUTCTimeStamp()),
											 		'time' => date("H:i:s", $this -> currentTime -> getUTCTimeStamp())));
													
													
		$this -> json -> outputJqueryJSONObject('finishedForm', $this -> msg -> errorTicketSuccessSubmitted());
		//$this -> json -> outputJqueryJSONObject('finishedForm', $this -> userIDNumber);
		
		
	}
	
	public function validateErrorCategory() {
		$duplicateCategory = $this -> db -> prepare('SELECT ErrorCategory FROM ErrorReportsCategories WHERE ErrorCategory = ?');
		$duplicateCategory -> execute(array($this -> errorType));
		
		if($this -> validate -> emptyInput($this -> errorType)) {
			$this -> json -> outputJqueryJSONObject('emptyCategory', $this -> msg->flashMessage('message error', $this -> msg -> emptyCategoryMessage()));
			return false;
		} else if(count($duplicateCategory -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('duplicateCategory', $this -> msg->flashMessage('message error', $this -> msg -> duplicateCategory()));
			return false;
		}
		return true;
	}
	
	public function AddCategory() {
		$this -> db -> insert('ErrorReportsCategories', array('ErrorCategory' => $this -> errorType));
		$this -> json -> multipleJSONOjbects(array("success" => $this -> msg -> flashMessage('message success', $this -> msg -> categoryAdded()),
													   "path"	 => PATH,
													   "id"		 => $this -> db -> lastInsertId()));
	} 

	public function deleteCategory() {
		$this -> json -> outputJqueryJSONObject('categoryDeleted', $this -> msg->flashMessage('message generic', $this -> msg -> categoryDeleted()));
		$sth = $this -> db -> prepare("DELETE FROM ErrorReportsCategories WHERE id = :id");
		$sth -> execute(array(':id' => $this->categoryID));
	}
	
	public function delete() {
		$this -> msg -> set($this -> msg->flashMessage('message error', $this -> msg -> errorDeleted()));
		$sth = $this -> db -> prepare("DELETE FROM ErrorReports WHERE id = :id");
		$sth -> execute(array(':id' => $this -> errorID));
		$this -> redirect -> redirectPage('admin/errors?page=1');
	}
	

}