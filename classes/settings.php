<?php

class Settings extends BaseObject {
	
	public $TimeZone;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    private static $singletonSettings;
    private static $initialValue = 0;
    public static function GetSettings(){
        if(empty(self::$singletonSettings)){
            $s = new Settings();
            $sth = $s -> db -> prepare('SELECT * FROM AccountSettings WHERE id = :id');
            $sth->execute(array(':id' => 1));
            $record = $sth -> fetch();
            $s->fill($record);
            self::$singletonSettings = $s;
        }
        return self::$singletonSettings;
    }
	
	protected function fill(array $row){
		$this -> TimeZone = $row['TimeZone'];
    }
	
	public function getTimeZone() {
		return $this -> TimeZone;
	}
	
	

}