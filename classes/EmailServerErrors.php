<?php


class EmailServerError extends BaseObject {
	
	public $message;
	public $type;

	
	public function SendMessage() {
			
		$time = date("M. j Y / g:i a"); 
		$ip = getenv('REMOTE_ADDR');
		$userAgent = getenv('HTTP_USER_AGENT');
		$referrer = getenv('HTTP_REFERER');
		$query = getenv('QUERY_STRING');
 
			//COMBINE VARS INTO OUR LOG ENTRY
		$StripeErrorMessage = 
			   $this -> type .
			   "\nIP: " . $ip . 
			   "\nTIME: " . $time . 
			   "\nREFERRER: " . $referrer . 
			   "\nSEARCH STRING: " . $query . 
			   "\nUSER AGENT: " . $userAgent . 
			   "\nMESSAGE: " . $this -> message . "\n\n";
		
		Log::Errors($StripeErrorMessage);
		
			
		
	}
	

}