<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class User extends BaseObject {
	
	private $sessionUserID;
	public $_userId;
	public $firstName;
	public $lastName;
	public $userName;
	public $startDate;
	public $starTime;
	public $email;
	public $password;
	public $confimPassword;
	public $salt;
	public $securityToken;
	public $userType;

    public function __sleep()
    {
        parent::__sleep();
		return array('_userId', 'password', 'confimPassword');
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


    public function __construct(){
    	
        parent::__construct();
    }

    public static function WithID($userId){
        $instance = new self();
        $instance->_userId = $userId;
        $instance->loadById();
        return $instance;
    }
	
    protected function loadByID()
    {
        $sth = $this -> db -> prepare('SELECT * FROM users WHERE userId = :userId');
        $sth->execute(array(':userId' => $this->_userId));
        $record = $sth -> fetch();
        $this->fill($record);
    }

	
    protected function fill(array $row){
    	$this -> _userId = $row['userId'];
        $this -> firstName = $row['firstName'];
		$this -> lastName = $row['lastName'];
		$this -> startDate = $row['date'];
		$this -> starTime = $row['time'];
		$this -> email = $row['email'];
		$this -> userName = $row['UserName'];
		$this -> salt = $row['salt'];
		$this -> password = $row['PassWord'];
		if(!empty($row['securityToken'])) {
			$this -> securityToken = $row['securityToken'];	
		}
    }


	public function getFullName() {
		return $this -> firstName . ' ' . $this -> lastName;
	}

	private function adminUserLogin() {
		$sth = $this->db->prepare("SELECT * FROM users WHERE UserName = :username AND isAdminUser=1");
		$sth->execute(array(':username' => $this -> userName));	
		
		$data = $sth->fetch();							 
		$count =  $sth->rowCount();
		
		if ($count > 0) {
			
			$userLogin = User::WithID($data['userId']);
            if($userLogin -> ValidatePasswordAttempt($this -> password)) {
				// login	
				Session::init();
            	Session::set('adminLoggedIn', true);
            	//Session::set('user', User::WithID($data['userId']));	
				//redirect to main app section
				
				$this -> json -> outputJqueryJSONObject('redirect', $this -> redirect -> JSONRedirect('admin/users?page=1'));	
				
			} else {
					$this -> json -> outputJqueryJSONObject('adminErrorLogin', $this -> msg->flashMessage('message error', $this -> msg -> wrongCredentialMatch(). " No Username"));
			}
		} else {
			//show an error
			$this -> json -> outputJqueryJSONObject('adminErrorLogin', $this -> msg->flashMessage('message error', $this -> msg -> wrongCredentialMatch(). " Not working"));
		}
	}
	
	private function applicationUserLogin() {
		$sth = $this->db->prepare("SELECT * FROM users WHERE UserName = :username");
		$sth->execute(array(':username' => $this -> userName));
		
		$data = $sth->fetch();							 
		$count =  $sth->rowCount();
		
		if ($count > 0) {
			
			$userLogin = User::WithID($data['userId']);
            if($userLogin -> ValidatePasswordAttempt($this -> password)) {
				// login	
				Session::init();
            	Session::set('loggedIn', true);
            	Session::set('user', User::WithID($data['userId']));	
				//redirect to main app section
				$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> recipeMainApp());	
				
			} else {
				$this -> json -> outputJqueryJSONObject('loginErrorMessage', $this -> msg -> wrongCredentialMatch(). " No Username");
			}
		} else {
			$this -> json -> outputJqueryJSONObject('loginErrorMessage', $this -> msg -> wrongCredentialMatch());			
		}
		
	}
	
	public function userLogin($adminPage = false) {
		if ($adminPage) {
			$this -> adminUserLogin();
		} else {
			$this -> applicationUserLogin();
		}
	}
	
	public function validateNewUser() {
		$emailCheck = $this -> db -> prepare('SELECT email FROM users WHERE email = ? AND isAdminUser = 0');
		$emailCheck -> execute(array($this -> email));
		
		$passwordCheck = $this -> db -> prepare('SELECT UserName FROM users WHERE UserName = ? AND isAdminUser = 0');
		$passwordCheck -> execute(array($this -> userName));	

		//no first name entered
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('firstName', $this -> msg -> isRequired("First Name"));
			return false;
			//not using letters
		} else if($this -> validate -> notUsingLetters($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('firstNameNoLetters', $this -> msg -> OnlyLetters("First Name"));
			return false;
			//no last name entered		
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('lastName', $this -> msg -> isRequired("Last Name"));
			return false;
			//last name not using letters
		} else if($this -> validate -> notUsingLetters($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('lastNameNoLetters', $this -> msg -> OnlyLetters("Last Name"));	
			return false;
			//no username entered
		} else if($this -> validate -> emptyInput($this -> userName)) {
			$this -> json -> outputJqueryJSONObject('userName', $this -> msg -> isRequired("Username"));
			return false;
			//no spaces check
		} else if($this -> validate -> noWhiteSpace($this -> userName)) {
			$this -> json -> outputJqueryJSONObject('userNameNoWhiteSpace', "Username: " . $this -> msg -> noWhiteSpace());
			return false;
			// email is required
		} else if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('Email', $this -> msg -> isRequired("Email"));
			return false;
			//email verification 
		} else if($this -> validate -> correctEmailFormat($this -> email)) {
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> emailFormatRequiredMessage());
			return false;
			//duplicate email check
		} else if(count($emailCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('duplicateEmail', $this -> msg -> duplicateEmail());
			return false;
			//empty password
		} else if ($this -> validate -> emptyInput($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('password', $this -> msg -> isRequired("Password"));
			return false;
			//no white space: password
		} else if($this -> validate -> noWhiteSpace($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('passwordNoWhiteSpace', $this -> msg -> flashMessage('message error', $this -> msg -> noWhiteSpace()));
			return false;
			//only letters numbers
		} else if($this -> validate -> onlyLettersNumbers($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('passwordOnlyLettersNumbers', $this -> msg -> onlyLettersNumbers());
			return false;
			//min password check
		} else if ($this -> validate -> passwordLength($this->_initialPassword, 6)) {
			$this -> json -> outputJqueryJSONObject('minPasswordLength', $this -> msg -> passwordLengthMessage("6"));
			return false;
			//password match check
		} else if ($this -> validate -> notMatchingPasswords($this -> confimPassword, $this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('confirmPasswordCheck', $this -> msg -> notMatchingPasswordsMessage());	
			return false;
			//duplicated username	
		} else if (count($passwordCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('duplicateUserName', $this -> msg -> duplicateUsername());	
			return false;
			//no errors create new user
		}
		return true;
	}
	
	public function newUser($loginPage = false) {
		$this -> db -> insert('users', array('firstName' => $this -> firstName, 
							  				 'lastName' => $this -> lastName,
							  				 'email' => $this -> email,
							  				 'UserName' => $this -> userName,
											 'PassWord' => $this-> password, 
											 'date' => date("Y-m-d", $this -> currentTime -> getUTCTimeStamp()),
											 'time' => date("H:i:s", $this -> currentTime -> getUTCTimeStamp()),
											 'salt' => $this->salt));
	
		$newUserID = $this -> db -> lastInsertId();
		
		Session::init();
        Session::set('loggedIn', true);
        Session::set('user', User::WithID($newUserID));
											 									 
		$this -> createUserCategory($newUserID);
				
		Folder::CreateUserImageFolder($newUserID);
		Folder::CreateUserImageFolder($newUserID. '/profile');
						
		Email::Credentials($this -> email, $this -> userName, $this -> _initialPassword, true);
		$this -> json -> outputJqueryJSONObject('redirect', $this -> pages -> recipeMainApp());
	}


	

	private function createUserCategory($userID) {
		$this -> db -> insert('RecipeCategory', array('userID' => $userID,
													  'categoryName' => 'Uncategorized'));
	}

    public function ValidatePasswordAttempt($pass)
    {
        return Hash::create('sha256', $pass, $this -> salt) == $this -> password;
    }

    private $_password;
    private $_initialPassword;
    public function SetPassword($password)
    {
        $this->salt = Hash::generateSalt();
        $this->_initialPassword = $password;
        $this -> password = Hash::create('sha256', $password, $this->salt);
    }
    
    public function validateEmail() {
		$getCredentials = $this -> db -> prepare("SELECT * FROM users WHERE email = :email AND isAdminUser = :userType");
		$getCredentials -> execute(array(":email" => $this -> email,
										 ":userType" => $this -> userType));
		$getCredentialObject = $getCredentials -> fetch();
		
		$this->_userId = $getCredentialObject['userId'];
		
		if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg -> emailRequiredRetriveCredentials());
			return false;
		} else if(!Email::verifyEmailAddress($this -> email)) {
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> validate -> noEmailMatch($this -> email, $getCredentialObject['email'])) {
			$this -> json -> outputJqueryJSONObject('errorEmail', $this -> msg -> emailNotMatchingMessage());
			return false;
		} 
		return true;	
			
		
	}
    
    
   	public function SendEmail() {
   						
   		$securityToken = Hash::generateSecurityToken();
		
		$this -> json -> outputJqueryJSONObject('validEmail', true);
			
		$this->db->update('users', array('securityToken' => $securityToken), array('userId' => $this->_userId));			
   				
   		$resetLink = PATH . 'login/passwordReset?token=' . $securityToken . '&userID=' . $this->_userId;
   		
   	
   		Email::ResetPassword($this -> email, $resetLink);
   	}
   
	public function validatePassword() {
		if($this -> validate -> emptyInput($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('newPassword', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Password")));
			return false;
		} else if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
			$this -> json -> outputJqueryJSONObject('newPasswordLength', $this -> msg -> flashMessage('message error', $this -> msg -> passwordLengthMessage("6")));
			return false;
		}
		return true;
   	}
	
	public function SaveNewPassword($passwordReset = false) {
		
		$this -> json -> outputJqueryJSONObject('redirect', PATH . 'login/userLogin');
		$this -> msg->set($this -> msg -> flashMessage('message success', "Your password has been reset, please login"));
		
		$postData = array('PassWord' => $this -> password, 'salt' => $this -> salt, 'securityToken' => '');
				
		$this -> db -> update('users', $postData, array('userId' => $this->_userId));
	}
	
	private function adminUserDelete() {
		$deleteUser = $this -> db -> prepare("DELETE FROM users WHERE userId = :userId");
		$deleteUser -> execute(array(':userId' => $this -> _userId));
		
		$deleteCategories = $this -> db -> prepare("DELETE FROM RecipeCategory WHERE userID = :userID");
		$deleteCategories -> execute(array(':userID' => $this -> _userId));
		
		$deleteRecipes = $this -> db -> prepare("DELETE FROM recipes WHERE userID = :userID");
		$deleteRecipes -> execute(array(':userID' => $this -> _userId));
		
		//delete user folder
		Folder::DeleteUserDirectory('view/recipes/images/' . $this -> _userId);
		
		$this -> msg -> set($this -> msg -> flashMessage('message generic', $this -> msg -> userDeletedMessage()));
		$this -> redirect -> redirectPage('admin/users?page=1');
	}
	
	public function userSideDeleteAccount() {
		$this -> sessionUserID = Session::User() -> _userId;
		$deleteUser = $this -> db -> prepare("DELETE FROM users WHERE userId = :userId");
		$deleteUser -> execute(array(':userId' => $this -> sessionUserID));
		
		$deleteCategories = $this -> db -> prepare("DELETE FROM RecipeCategory WHERE userID = :userID");
		$deleteCategories -> execute(array(':userID' => $this -> sessionUserID));
		
		$deleteRecipes = $this -> db -> prepare("DELETE FROM recipes WHERE userID = :userID");
		$deleteRecipes -> execute(array(':userID' => $this -> sessionUserID));
		
		//delete user folder
		Folder::DeleteUserDirectory('view/recipes/images/' . $this -> sessionUserID);
		//destroy session
		Session::destroy();
		//go back to home
		$this -> redirect -> redirectPage("home");
	}
	
	public function deleteAccount($admin = false) {
		if($admin) {
			$this -> adminUserDelete();
		} else {
			$this -> userSideDeleteAccount();
		}
		
		
	}

	public function validateAccount() {		
		if($this -> validate -> emptyInput($this  -> firstName)) {
			$this -> json -> outputJqueryJSONObject('emptyFirstName', $this -> msg -> isRequired("First Name"));
			return false;
		} else if($this -> validate -> notUsingLetters($this  -> firstName)) {
			$this -> json -> outputJqueryJSONObject('onlyLettersFirstName', $this -> msg -> OnlyLetters("First Name"));
			return false;
		} else if ($this -> validate -> emptyInput($this  -> lastName)) {
			$this -> json -> outputJqueryJSONObject('emptyLastName', $this -> msg -> isRequired("Last Name"));
			return false;
		} else if($this -> validate -> notUsingLetters($this  -> lastName)) {
			$this -> json -> outputJqueryJSONObject('onlyLettersLastName', $this -> msg -> OnlyLetters("Last Name"));
			return false;
		} else if ($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('emptyEmailMessage', $this -> msg -> isRequired("Email"));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> email)) {
			$this -> json -> outputJqueryJSONObject('veryifyUserEmail', $this -> msg -> emailFormatRequiredMessage());
			return false;
		} else if($this -> validate -> emptyInput($this -> userName)) {
			$this -> json -> outputJqueryJSONObject('emptyUserNameMessage', $this -> msg -> isRequired("Username"));
			return false;
		} else if($this -> validate -> noWhiteSpace($this -> userName)) {
			$this -> json -> outputJqueryJSONObject('userNameNoWhiteSpace', "Username: ". $this -> msg -> noWhiteSpace());
			return false;
		} else if(!empty($this -> _initialPassword)) {
			if($this -> validate -> noWhiteSpace($this -> _initialPassword)) {
				$this -> json -> outputJqueryJSONObject('passwordNoWhiteSpace', "Password: ".$this -> msg -> noWhiteSpace());
				return false;
			} else if ($this -> validate -> passwordLength($this -> _initialPassword, 6)) {
				$this -> json -> outputJqueryJSONObject('minPasswordLength', $this -> msg -> passwordLengthMessage("6"));
				return false;
			} else if ($this -> validate -> notMatchingPasswords($this -> confimPassword, $this->_initialPassword)) {
				$this -> json -> outputJqueryJSONObject('confirmPasswordCheck', $this -> msg -> notMatchingPasswordsMessage());
				return false;
			}
		}
		return true;
	}

	public function SaveCredentials() {
		if(!empty($this -> _initialPassword)) {
			$postData = array(
				'firstName' => $this  -> firstName,
				'lastName' => $this  -> lastName,
				'email' => $this -> email,
				'UserName' => $this -> userName,
				'PassWord' => $this -> password, 
				'salt' => $this -> salt
			);
		} else {
			$postData = array(
				'firstName' => $this  -> firstName,
				'lastName' => $this  -> lastName,
				'email' => $this -> email,
				'UserName' => $this -> userName
			);
		}
		
		
				
		//send to database
		$where = array('userId' => $this->_userId);	
		$this -> db -> update('users', $postData, $where);
			
		$this -> json -> outputJqueryJSONObject('AccountUpdated', $this -> msg -> AccountUpdate());
		
		
		
		
	}
    
	

}