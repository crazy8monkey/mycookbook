<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Category extends BaseObject {
	private $_userId;	

	public $categoryID;
	public $categoryName;
	public $currentCategoryID;

    public function __sleep() {
        parent::__sleep();	
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
    	$this -> _userId = Session::User() -> _userId;
        parent::__construct();
    }

	public static function WithID($categoryID){
        $instance = new self();
        $instance -> categoryID = $categoryID;
        $instance -> loadById();
        return $instance;
    }
	
	 protected function loadById() {
        $sth = $this -> db -> prepare('SELECT * FROM RecipeCategory WHERE category_id = :category_id');
        $sth->execute(array(':category_id' => $this -> categoryID));
        $record = $sth -> fetch();
        $this -> fill($record);
    }
	 
	protected function fill(array $row){
		$this -> categoryName = $row['categoryName'];
    }
	

	
	public function validateCategory() {
		$duplicateCategory = $this -> db -> prepare('SELECT categoryName FROM RecipeCategory WHERE userID = ' . $this -> _userId . ' AND categoryName = ?');
		$duplicateCategory -> execute(array($this -> categoryName));
		
		if(count($duplicateCategory -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('DuplicateCategory', $this -> msg -> flashMessage('message error', $this -> msg -> duplicateCategory()));
			return false;
		} else if($this -> validate -> emptyInput($this -> categoryName)) {
			$this -> json -> outputJqueryJSONObject('EmptyNewCategory', $this -> msg -> flashMessage('message error', $this -> msg -> emptyCategoryMessage()));
			return false;
		} else if($this -> validate -> noWhiteSpace($this -> categoryName)) {
			$this -> json -> outputJqueryJSONObject('NewCategoryOneWord', $this -> msg -> flashMessage('message error', $this -> msg -> categoryOneWordMessasge()));
			return false;
		} else if($this -> validate -> notUsingLetters($this -> categoryName)) {
			$this -> json -> outputJqueryJSONObject('NewCategoryOnlyLetters', $this -> msg -> flashMessage('message error', $this -> msg -> onlyLetters()));
			return false;		
		}
		return true;
	}
	

	
	public function Save() {
		if(isset($this -> categoryID)) {
			
			
			$postData = array('categoryName' => $this -> categoryName);
			
			$this -> db -> update('RecipeCategory', $postData, array('category_id' => $this -> categoryID));
			
			$this -> json -> multipleJSONOjbects(array("editCategory"	=> $this -> msg -> flashMessage('message success', $this -> msg -> categoryEdited()), 
													   "changeUrlPath"	=> $this -> redirect -> JSONRedirect('recipes/category/' . $this -> categoryName)));
			
		} else {
			$this -> db -> insert('RecipeCategory', array('categoryName' => $this -> categoryName,
														  'userID' => $this -> _userId));
			$this -> json -> multipleJSONOjbects(array("newCategory"	=> $this -> msg -> flashMessage('message success', $this -> msg -> categoryAdded()), 
													   "lastInsertedId"	=> $this -> db -> lastInsertId()));	
		}
		
		
	}

	public function deleteCategory() {
		//change category to Uncategorized
		// get to uncategoryized id
		$changeCategory = $this -> db -> prepare("UPDATE recipes SET categoryID = ". $this -> _userId . " WHERE categoryID = " . $this -> categoryID);
		$changeCategory -> execute();
	
		$sth = $this -> db -> prepare("DELETE FROM RecipeCategory WHERE category_id = :category_id");
		$sth -> execute(array(':category_id' => $this -> categoryID));
		
		$this -> msg->set($this -> msg->flashMessage('message generic', $this -> msg -> categoryDeleted()));
		$this -> redirect -> redirectPage("recipes");
	}


	public function changeCategory() {
		if($this -> categoryID == "All Recipes") {
			$this -> json -> outputJqueryJSONObject('redirect', PATH . "recipes");
		} else {
			$this -> json -> outputJqueryJSONObject('redirect', PATH . "recipes/category/" .$this -> categoryID);
		}
	}

	
    

}