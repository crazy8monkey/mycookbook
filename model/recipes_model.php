<?php

class Recipes_Model extends Model {
	
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('loggedIn');
	}
		
	public function recipeCategorySelected($category) {
		$getCategoryID = $this -> db -> prepare("SELECT * FROM RecipeCategory WHERE categoryName = :name");
		$getCategoryID -> execute(array(":name" => $category));
		$getCategoryIDObject = $getCategoryID -> fetch();
				
		return $this -> db -> select("SELECT * FROM recipes INNER JOIN RecipeCategory ON recipes.categoryID = RecipeCategory.category_id  WHERE recipes.categoryID = " . $getCategoryIDObject['category_id']);	

	}	

	


}