<?php

Class Admin_Model extends Model {

	public $timeZone;
	
	public function __construct() {
		Session::init();
		parent::__construct();
		$this -> timeZone = $this -> settings -> getTimeZone();
	}


	
	
	public function allUserList() {
		$userTable = "users";
		$count = $this -> db -> query("SELECT COUNT(*) FROM ". $userTable) -> fetchColumn();
		
		require 'libs/Pagination.php';
		
		$pagination = new Pagination();
		$userLimitDatabase = "10";
		$page = $_GET['page'];
		if($page) {
			//First Item to dipaly on this page
			$start = ($page - 1) * $userLimitDatabase;
		}
		else {
			//if no page variable is given, set start to 0
			$start = 0;
		}
		$userTargetPage = PATH . "admin/users"; 
		//calls the select function in the database
		$users =  $this -> db -> select('SELECT * FROM '. $userTable . ' WHERE isAdminUser = 0 ORDER BY userId DESC LIMIT ' . $start. ', ' . $userLimitDatabase);
		$userPagination = $pagination -> pagination($count, $userLimitDatabase, $userTargetPage);
		return $users;
	}

	public function errorList() {
		$errorTable = "ErrorReports";
		$count = $this -> db -> query("SELECT COUNT(*) FROM ". $errorTable) -> fetchColumn();
		
		require 'libs/Pagination.php';
		$pagination = new Pagination();
		
		$errorLimitDatabase = "10";
		$page = $_GET['page'];
		if($page) {
			//First Item to dipaly on this page
			$start = ($page - 1) * $errorLimitDatabase;
		}
		else {
			//if no page variable is given, set start to 0
			$start = 0;
		}
		$errorTargetPage = PATH . "admin/errors"; 
		//calls the select function in the database
		$errors = $this -> db -> select('SELECT * FROM '. $errorTable . ' ORDER BY id LIMIT ' . $start. ', ' . $errorLimitDatabase);
		$userPagination = $pagination -> pagination($count, $errorLimitDatabase, $errorTargetPage);
		return $errors;
	}




	public function errorCategoryList() {
		return $this -> db -> select('SELECT * FROM ErrorReportsCategories');
	}
	
	public function errorCatoryEdit($category) {
		$postData = array('errorType' => $category['ticketCategory']);
	
		$this -> json -> outputJqueryJSONObject('categoryChanged', $this -> msg->flashMessage('message generic', $this -> msg -> categoryEdited()));
		//calls the database class -> calls the update method
		$this -> db -> update("ErrorReports", $postData, "`id` = {$category['id']}");
	}
	
}
?>
