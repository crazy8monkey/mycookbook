<?php

class Login_Model extends Model {
	
	public function __construct() {
		parent::__construct();
	}
	

	public function sendAdminCredentials($AdminemailInput) {
		
		$getAdminCredentials = $this -> db -> prepare("SELECT * FROM AdminUsers WHERE Email = :Email");
		$getAdminCredentials -> execute(array(":Email" => $_POST['admin-email']));
		$getCredentialObject = $getAdminCredentials -> fetch();
		
		if($this -> validate -> noEmailMatch($AdminemailInput, $getCredentialObject['Email'])) {
			$this -> json -> outputJqueryJSONObject('errorEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailNotMatchingMessage()));
		} else if(!Email::verifyEmailAddress($getCredentialObject['Email'])) {
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
		} else if($this -> validate -> emptyInput($AdminemailInput)) {
			$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailRequiredRetriveCredentials()));
		} else {
			Email::SendEmail($getCredentialObject['Email'], 'Admin Credentials', 'MyCookBook:Administration', $getCredentialObject['Username'], $getCredentialObject['Password']);
			$this -> json -> outputJqueryJSONObject('redirect', $this -> redirect -> JSONRedirect('login/AdminCredentialsSent'));
		}
	}


	
}