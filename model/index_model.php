<?php

Class Index_Model extends Model {
	
	public function __construct() {
		parent::__construct();		
	}
	
	public function userLogin() {
		$sth = $this->db->prepare("SELECT * FROM users WHERE 
							UserName = :username AND PassWord = :password");
		$sth->execute(array(':username' 	 => $_POST['username'],
							':password'  => Hash::create(HASH_GENERAL_KEY, $_POST['password'], HASH_PASSWORD_KEY)
							));
		$data = $sth->fetch();	
								 
								 
		$count =  $sth->rowCount();
		if ($count >0)  
		{
			// login		
			Session::startSession(true, $data['userImageFolder'], $data['userRecipeTable'], $data['userRecipeCategory'], $data['firstName'], $data['lastName'], $data['email']);
			//redirect to main app section
			$this -> redirect -> redirectPage('recipes');
		} else {
			//show an error
			$this -> msg -> set($this -> msg->flashMessage('message error', $this -> msg ->  wrongCredentialMatch()));
			$this -> redirect -> redirectPage('login/userLogin');
		}
	}
	
	
}

?>