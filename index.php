<?php

require 'config/paths.php';
require 'config/database.php';
require 'config/globals.php';
require 'util/Auth.php';


//use an autoloader
function __autoload($class) {
	require LIBS . $class. '.php';
	require_once CLASSES . 'init.php';
	require_once LISTS . 'init.php';
}

// Load the Bootstrap
$bootstrap = new Bootstrap();
// Optional Path Settings
//$bootstrap->setControllerPath();
//$bootstrap->setModelPath();
//$bootstrap->setDefaultFile();
//$bootstrap->setErrorFile();

$bootstrap->init();