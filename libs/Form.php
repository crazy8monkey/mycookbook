<?php

class Form {
 		
 
 	//form label
 	private function label($label) {
 		$fieldLabel = "";	
			
 		if(!empty($label)) {
			$fieldLabel = "<div class='formLabel'>". $label ."</div>";
		}
		
 		return $fieldLabel;
 	}
	
 	public function TextInput($label, $name, $password = false) {
 		$textboxType = "";
		
		$angularBinding = "";
		
		if ($password) {
			$textboxType = "password";
		} else {
			$textboxType = "text";
		}
	
		
		
		return "<div class='inputLine'>". $this -> label($label) ."<input type='". $textboxType . "' name='" .$name . "' ng-model='" .$name . "' /></div>";	 
	}
	
	public function DropDown($label, $name) {
		return "<div class='inputLine'>". $this -> label($label) ."<div class='mui-select'><select name='" .$name ."' ng-init='categories'><option value='0'>Please Select</option><option ng-repeat='x in categories' value='{{x.id}}'>{{x.ErrorCategory}}</option></select></div></div>";	 
	}
	public function TextArea($label, $name, $id = false) {
		return "<div class='inputLine'>". $this -> label($label) ."<textarea name='". $name . "'></textarea></div>";
	}
	
 
}