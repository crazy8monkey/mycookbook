<?php

class Model {

	function __construct() {
		//database connection
		
		$this -> msg = new Message();
		$this -> validate = new Validation();
		$this -> redirect = new Redirect();
		$this -> CSRF = new CSRF();
		$this -> settings = Settings::GetSettings();
		$this -> currentTime = new Time();
		$this -> db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
		
		
	}

}