<?php

Class Folder {
	
	public static function DeleteUserDirectory($dir) {
		if ($handle = opendir($dir)) {
			$array = array();
 
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if(is_dir($dir.$file))
					{
					if(!@rmdir($dir.$file)) // Empty directory? Remove it
					{
						self::DeleteUserDirectory($dir.$file.'/'); // Not empty? Delete the files inside it
					}
				}
				else
				{
				@unlink($dir.$file);
				}
			}
		}
		closedir($handle);
		 
		@rmdir($dir);
		}
	}
	
	public static function CreateUserImageFolder($newFolder) {
		//make a new directory folder
		$newUserRecipeImages = 'view/recipes/images/' . $newFolder . '/';
		
		mkdir($newUserRecipeImages);
		//if new user recipe folder exists
		if(is_dir($newUserRecipeImages)) {
			$defaultImageFolder = $newUserRecipeImages . 'default-image/'; 
			mkdir($defaultImageFolder);
			//if default image folder exists....copy image
			if(is_dir($defaultImageFolder)) {
				copy(IMAGES_PATH . 'default-image.png', $defaultImageFolder . 'default-image.png');	
			}
			
		}	
	}
}
