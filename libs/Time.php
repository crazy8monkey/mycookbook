<?php

//http://www.pontikis.net/tip/?id=18

class Time {
	
	private $TimeZone;
	
	public function __construct() {
		date_default_timezone_set('UTC');		
	}	
		
	public function getUTCTimeStamp() {
		$time = new DateTime("now");
		return $time -> getTimestamp() + $time -> getOffset();
	}
	
	public function formatDate($value, $timeZone) {
		$date = DateTime::createFromFormat('Y-m-d', $value);
		$date -> setTimezone(new DateTimeZone($timeZone));
		$formatedDate = $date->format('F j, Y');
		return $formatedDate;
	}
	
	public function formatTime($value, $timeZone) {
		$date = DateTime::createFromFormat('H:i:s', $value);
		$date -> setTimezone(new DateTimeZone($timeZone));
		$formatedDate = $date->format('g:i a');
		return $formatedDate;
		
	}
	
	
	
}

?>