<?php

class Pages {
	//index controller
	public function Home() {return PATH;}
	
	//login controller
	public function LoginController() {return PATH . "login";}
	public function signup() {return PATH . "login/signup";}
	public function userLogin() {return PATH . "login/login";}
	public function sendCredentialsPostBack() {return PATH. "login/sendCredentials";}
	public function resetPasswordPostBack() {return PATH. "login/resetPasswordForm";}
	public function signupUserPostBack() {return PATH. "login/newUser";}
	
	//recipe controller
	public function recipeMainApp() {return PATH . "recipes"; }
	public function Save($type) {return PATH . "recipes/save/" . $type; }
	public function RecipeSingle($id) {return PATH . "recipes/viewrecipe?id=" . $id;}
	public function logout() {return PATH . "recipes/logout"; }
	
	//error controller
	public function errorMainPage()  {return PATH . "error?suggestions=true";}
	public function submitErrorPostBack() {return PATH . "error/insertError";}
	
	
}