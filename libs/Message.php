<?php

class Message {
 
	public function set($message) {
 		if( !session_id() ) session_start();
		$_SESSION['flash'] =  $message;
		return true;
 
	}
 
	public function show() {
		if (!empty($_SESSION['flash'])) {
 			echo $_SESSION['flash'];
			unset($_SESSION['flash']);
		}
		return true; 
	}
	public function flashMessage($cssClass, $message) {
		//<a href='javascript:void(0);' onclick='Globals.closeFlashMessage()'><div class='message-close'></div></a>
		return "<div class='". $cssClass . "'>". $message ."</div>";
	}
	
	public function pageFlashMessage($angularBinding) {
		return "<div class='row pageMessage' ". $angularBinding . "><div class='col-md-12'><div class='loadingSpinner'><img src='" . PATH . "public/images/ajax-loader.gif' /></div><div class='text'></div></div></div>";
	}
	
		
	//universal
	public function emailFormatRequiredMessage() {return 'Needs to be in Email format.';}
	public function onlyLetters($label = false) {
		if($label) {
			return $label . ': Only letters are allowed.';	
		} else {
			return 'Only letters are allowed.';
		}
		
	}
	public function isRequired($requiredField) { return $requiredField. ' is required.';}
	public function noWhiteSpace() {return 'Spaces are not allowed';}
	public function AccountUpdate() {return 'Your account has been saved.';}
	
	
	//recipe messages
	public function duplicateRecipeName() {return 'Recipe name has already been entered, please enter a different name';}
	public function recipeNameRequired() {return 'Recipe name is required.';}
	public function recipeAdded() {return 'Recipe Added.';}
	public function recipeDeleted() {return 'Recipe Deleted.';}
	public function recipeChanged() {return 'Recipe changed.';}
	
	//settings messages
	public function credentialsUpdated() {return 'Credentials have been updated.';}
	public function fullNameUpdated() {return 'Full name updated.';}
	public function passwordUpdated() {return 'Password has been updated.';}
	
	//signup messages
	public function duplicateUsername() {return 'There is already a username associated with a different account.';}
	public function duplicateEmail() {return 'There is already an email associated with another account.';}
	public function passwordLengthMessage($length) {return 'Password must be ' . $length .' characters long.';}
	public function notMatchingPasswordsMessage() {return 'Passwords do not match.';}
	public function onlyLettersNumbers() {return 'Only letters and numbers';}
	
	//login messages
	public function wrongCredentialMatch() {return 'Wrong Username/Email and password combination.';}
	
	//retrieve credentials
	public function emailNotMatchingMessage() {return 'Email does not match our records.';}
	public function emailRequiredRetriveCredentials() {return 'Submitting an email is required to retrieve your credentials.';}
	
	//category messages
	public function categoryAdded() {return 'Category successfully entered.';}
	public function duplicateCategory() {return 'There is already a category entered into the database';}
	public function emptyCategoryMessage() {return 'Please enter a category';}
	public function categoryOneWordMessasge() {return 'Category must be one word';}
	public function categoryEdited() {return 'Category Edited.';}
	public function categoryDeleted() {return 'Category Deleted.';}

	//error page messages
	public function errorDescriptionMessasge() {return 'Error description is required.';}
	public function errorTypeMessage() {return 'Error type is required.';}
	public function errorTicketSuccessSubmitted() {return 'Thank you for subitting the form, the issue will be looked into shortly.';}
	public function errorDeleted() {return 'Error Deleted';}

	//admin section messages
	public function userDeletedMessage() {return 'User Deleted.';}
	
	
	
	
	
	
	
	
 
}