<?php

class Log
{
	
	/**
	 *
	 * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
	 * @param string $data The data to encode
	 * @param string $salt The salt (This should be the same throughout the system probably)
	 * @return string The hashed/salted data
	 */
	public static function Errors($msg)
	{
		//chmod($logfile, 777);
 
		$UserAgentTxt = fopen("errorLogging.txt", "a+");
		fwrite($UserAgentTxt, $msg);  
		fclose($UserAgentTxt); 
		
	}
	
}