<?php

Class Pagination {
	public $pagination;
	public function __construct() {}
	
	public function pagination($count, $limit, $targetpage) {
		$adjacents = 2;
		$page = isset($_GET['page']) ? max((int)$_GET['page'], 1) : 1;

		if ($page == 0)
			$page = 1;
		//previous page is page - 1
		$prev = $page - 1;
		//next page is page + 1
		$next = $page + 1;
		$lastpage = ceil($count / $limit);
		$this->pagination = "";
		if ($lastpage > 1) {
			$this->pagination .= "<div class='pagination'>";
			//previous button
			if ($page > 1)
				$this->pagination .= "<a href='$targetpage?page=$prev'><div class='pagination-left'>&lt;</div></a>";
			else
				$this->pagination .= "<div class='pagination-left'>&lt;</div>";

			//pages
			if ($lastpage < 7 + ($adjacents * 2))//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page)
						$this->pagination .= "<div class='pagination-number pagination-number-selected'>$counter</div>";
					else
						$this->pagination .= "<a href='$targetpage?page=$counter'><div class='pagination-number'>$counter</div></a>";
				}
			} elseif ($lastpage > 5 + ($adjacents * 2))//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if ($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
						if ($counter == $page)
							$this->pagination .= "<div class='pagination-number pagination-number-selected'>$counter</div>";
						else
							$this->pagination .= "<a href='$targetpage?page=$counter'><div class='pagination-number'>$counter</div></a>";
					}
					$this->pagination .= "...";
					$this->pagination .= "<a href='$targetpage?page=$lpm1'>$lpm1</a>";
					$this->pagination .= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";
				}
				//in middle; hide some front and some back
				elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

					$this->pagination .= "<a href='$targetpage?page=1'><div class='pagination-number'>1</div></a>";
					$this->pagination .= "<a href='$targetpage?page=2'><div class='pagination-number'>2</div></a>";
					$this->pagination .= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
						if ($counter == $page)
							$this->pagination .= "<span class='current'>$counter</span>";
						else
							$this->pagination .= "<a href='$targetpage?page=$counter'>$counter</a>";
					}
					$this->pagination .= "...";
					$this->pagination .= "<a href='$targetpage?page=$lpm1'>$lpm1</a>";
					$this->pagination .= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";
				}
				//close to end; only hide early pages
				else {
					$this->pagination .= "<a href='$targetpage?page=1'><div class='pagination-number'>1</div></a>";
					$this->pagination .= "<a href='$targetpage?page=2'><div class='pagination-number'>2</div></a>";
					$this->pagination .= "...";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page)
							$this->pagination .= "<div class='pagination-number pagination-number-selected'>$counter</div>";
						else
							$this->pagination .= "<a href='$targetpage?page=$counter'><div class='pagination-number'>$counter</div></a>";
					}
				}
			}
			//next button
			if ($page < $counter - 1)
				$this->pagination .= "<a href='$targetpage?page=$next'><div class='pagination-right'>&gt;</div></a>";
			else
				$this->pagination .= "<div class='pagination-right'>&gt;</div>";
			$this->pagination .= "</div>\n";
		}
	    echo $this->pagination;	
	}
}
