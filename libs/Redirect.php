<?php
Class Redirect {
	
	public function redirectPage($path) {
		
		if($path == "home") {
			return header('location:' . PATH);
		}
		else {
			return header('location:' . PATH . $path);
		}
		
	}
	
	public function JSONRedirect($path) {
		return PATH . $path;
	}
}
?>