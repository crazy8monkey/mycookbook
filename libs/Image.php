<?php
Class Image {
	
	public Static function deleteImage($imageName) {
		unlink($imageName);
	}
	/**
	 * renameImage
	 * @param imageFile $imageUploaded
	 * @param string $recipeName
	 * @return renamed image
	 */
	public static function renameImage($imageUploaded) {
		
		$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		
		
		$firstStageImage = $imageUploaded;
		$kaboom = explode(".", $firstStageImage);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		//image name change to recipe name
		$finalImage = substr(str_shuffle($alpha_numeric), 0, 20) . '.' . $fileExt;
		//preg_replace('#[^a-z.0-9]#i', '-', $recipeName . '.' . $fileExt);
		return $finalImage;
	}
	/**
	 * editRenameImage
	 * @param String current Image File $image
	 * @param string new recipe image file $newName
	 * @return renamed image
	 */
	public static function editRenameImage($image, $newName) {
		if(file_exists($image)) {
			if($image !== "default-image.png") {
				return rename($image, $newName);	
			}
		}
	}
	public static function getFileExt($image) {
		$fileExtStart = $image;
		$kaboom = explode(".", $fileExtStart);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		return $fileExt;
	}
	/**
	 * moveImage
	 * @param imageFile $imageUploaded
	 * @param imageFileTemp $recipeName
	 * @param session $imagePathDirectory
	 * @param string $recipeName
	 */
	public static function moveImage($image, $imageTemp, $ImagePathDirectory) {				
		$fileName = $image;
		$fileTmpLoc = $imageTemp;
		// Path and file name
		$pathAndName = $ImagePathDirectory.$fileName;
		// Run the move_uploaded_file() function here
		move_uploaded_file($fileTmpLoc, $pathAndName);
	}
	/**
	 * moveImage
	 * @param imageFile $image
	 * @param int $crop_width
	 * @param int $crop_height
	 * @param session $imageDirectory
	 * @param string $recipeName
	 */
	public static function cropImage($image, $crop_width, $crop_height, $imageDirectory) {
		
		$kaboom = explode(".", $image);
		// Split file name into an array using the dot
		$fileExt = end($kaboom);
		list($orig_width, $orig_height) = getimagesize($imageDirectory . $image);
		
		
		//adjust image width
		$resizedHeight = $orig_height * ($crop_width / $orig_width);
		
	   	$new_height = $crop_height;
	   	$new_width = $orig_width / ($orig_height / $crop_height);
		
		$centerImageY = round($new_height / 2);
		
		$tci = imagecreatetruecolor($crop_width, $crop_height);
		
		switch ($fileExt) {
			case 'jpg';
				$img = imagecreatefromjpeg($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, -$centerImageY, 0, 0, $crop_width, $resizedHeight, $orig_width, $orig_height);
				imagejpeg($tci, $imageDirectory . $image, 90);
				break;
			case 'jpeg';
				$img = imagecreatefromjpeg($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, -$centerImageY, 0, 0, $crop_width, $resizedHeight, $orig_width, $orig_height);
				imagejpeg($tci, $imageDirectory . $image, 90);
				break;	
			case 'gif';
				$img = imagecreatefromgif($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, -$centerImageY, 0, 0, $crop_width, $resizedHeight, $orig_width, $orig_height);
				imagegif($tci, $imageDirectory . $image);
				break;
			case 'png';
				$img = imagecreatefrompng($imageDirectory . $image);
			    imagecopyresampled($tci, $img, 0, -$centerImageY, 0, 0, $crop_width, $resizedHeight, $orig_width, $orig_height);
				imagepng($tci, $imageDirectory . $image);
				break;
		}		
	}
	/**
	 * validateImageWidth
	 * @param imageFile $image
	 * @param int $crop_width
	 * @param int $crop_height
	 * @param session $imageDirectory
	 * @param string $recipeName
	 */
	public static function getImageWidth($imageDirectoryFolder, $image) {
		list($width) = getimagesize($imageDirectoryFolder . $image);
		return $width;
	}
}//class image
?>