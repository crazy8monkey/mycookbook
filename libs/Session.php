<?php

class Session 
{
	//starts login session
	public static function init() {
		@session_start();
	}

	public static function set($key, $value) {
		$_SESSION[$key] = $value;
	}
	
	public static function get($key) {
		//if an empty session
		//return session 
		if (isset($_SESSION[$key])) return $_SESSION[$key];
	}
	
	public Static function User() {
		return $_SESSION['user'];
	}
	
	//ends login session
	public static function destroy() {
		session_destroy();
	}
	

}
