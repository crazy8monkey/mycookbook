<?php

Class Email {

	public static function emailHeader() {
		return "<tr><td style='padding: 0px 0px 10px 0px;' align='center'><img src='". PATH . "public/images/home-logo.png' border='0' width='220' height='48' /></td></tr>";
	}
	
	public static function BodyBackground() {
		return "padding:15px 0px 15px 0px; margin:0px; background: #ffdc2a;";
	}

	public static function tableStylesMain() {
		return "width:500px; border-collapse:collapse; border:0px; padding:0px; margin:0px auto 0px auto; font-family:arial;";
	}
	
	public static function copyrightYear() {
		return "<table style='width:500px; margin: 0 auto; color:#737373'><tr><td style='text-align: center; padding: 10px 0px 0px 0px;'>2013 - " . date("Y") . " &copy; MyCookBook. All Rights Reserved.</td></tr></table>";
	}
	
	public function tableStylesContent($content) {
		return "<table style='color:#737373; background:white; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; width: 100%; padding: 15px 20px 15px 20px;' cellpadding='0' cellspacing='0'>". $content. "</table>";
	}
	
	public static function Credentials($to, $username, $password, $showWelcomeMessage = false) {
		if($showWelcomeMessage == true) {
			$welcomeMessage	= "<tr><td style='border-bottom:1px solid #ECECEC; padding: 0px 10px 10px 0px;'>Welcome and thank you for joining to the MyCookBook community!</td></tr>";
			$padding = "padding: 10px 0px 0px 0px;";
		}
		else {
			$welcomeMessage="";
			$padding = "";
		}

		
		//http://stackoverflow.com/questions/3820631/using-php-mail-setting-correct-mime-type
		$header = 'From: MyCookBook'. "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
		
		$msg = "<html style='padding:0px; margin:0px;'><body style='". Email::BodyBackground(). "'>";
		$msg .="<table style='". Email::tableStylesMain() ."'>"; 
		$msg .= Email::emailHeader();
		$msg .= "<tr><td>" . 
				"<table style='color:#737373; background:white; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; width: 100%; padding: 15px 20px 15px 20px;' cellpadding='0' cellspacing='0'>".
		
				$welcomeMessage . "<tr><td style='". $padding ."'><span style='font-size:16px; font-weight:bold'>Your Credentials</span><br />" .
			"<table style='font-size:12px; padding:4px 0px 0px 0px' cellspacing='0' cellpadding='0'><tr><td style='font-weight:bold; width: 80px;'>username:</td><td>". $username ."</td></tr><tr><td style='font-weight:bold; width: 80px;'>password:</td><td>". $password ."</td></tr></table>" .			
			"</td></tr>".
			
			"</table>"
			 . 
			"</td></tr>";
		$msg .="</table>";
		$msg .= Email::copyrightYear();
		$msg .="</body></html>";
				
		
		
		mail($to, "MyCookBook User Credentials" , $msg, $header);
		
	}
	
	public static function ResetPassword($to, $resetLink) {

		//http://stackoverflow.com/questions/3820631/using-php-mail-setting-correct-mime-type
		$header = 'From: MyCookBook'. "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
		
		
		$msg = "<html style='padding:0px; margin:0px;'><body style='". Email::BodyBackground(). "'>";
		$msg .="<table style='". Email::tableStylesMain() ."'>"; 
		$msg .= Email::emailHeader();
		$msg .= "<tr><td>" . 
				"<table style='color:#737373; background:white; border-radius:5px; -webkit-border-radius:5px; -moz-border-radius:5px; width: 100%; padding: 15px 20px 15px 20px;' cellpadding='0' cellspacing='0'>".
		
				"<tr><td style='padding:0px;'><span style='font-size:16px; font-weight:bold'>Reset your Password</span><br />" .
			"<table style='font-size:12px; padding:4px 0px 0px 0px' cellspacing='0' cellpadding='0' width='100%'><tr><td>Please click this <a style='color: #737373; text-decoration:underline' href='" . $resetLink . "' target='_blank'>link</a> to reset your password</td></tr></table>" .			
			"</td></tr>".
			
			"</table>"
			 . 
			"</td></tr>";
		$msg .="</table>";
		$msg .= Email::copyrightYear();
		$msg .="</body></html>";
				
		
		
		mail($to, "Reset your password" , $msg, $header);
		
	}


	public static function verifyEmailAddress($emailInput) {
		return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$emailInput);
	}

}