<?php 

class View{
	
	
	public function __construct() {
		$this -> CSRF = new CSRF();
		$this -> pages = new Pages();
		$this -> form = new Form();
		$this -> msg = new Message();
		$this-> recordedTime = new Time();
	}
	
	
	public function render($name, $noInclude = false) {

		if ($noInclude == true) {
			require 'view/' . $name . '.php';
		} else {
			require 'view/header.php';
			require 'view/' . $name . '.php';
			require 'view/footer.php';
		}
		
	}
	
	
}
